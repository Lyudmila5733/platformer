﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;

public class RewardedAdsComponent : MonoBehaviour, IUnityAdsListener
{

    public event Action<string> OnAdsReady;
#if UNITY_IOS
    private string gameId = "3568039";
#elif UNITY_ANDROID
    private string gameId = "3568038";
#elif UNITY_STANDALONE_WIN
    private string gameId = "1234567";
#endif

    public string myPlacementId = "rewardedVideo";
//    private CellShop cellShop;
 //   private Item item;
    static bool listenerSet = false;
    public Action removeItemfromShopInventory;

    void Awake()
    {
        // Initialize the Ads listener and service:

        Advertisement.RemoveListener(this);
        if (!listenerSet)
        {
            Advertisement.AddListener(this);
            listenerSet = true;
        }
        Advertisement.Initialize(gameId, true);


    }

    private void Start()
    {
     //   GameManager.Instance.cellShop.updateShopUIInventory += RemoveItemfromShopInventory;

    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsReady(string placementId)
    {
    //    Debug.Log("cellShop1:" + cellShop);
        // If the ready Placement is rewarded, activate the button: 
        if (placementId == myPlacementId)
        {
            if (OnAdsReady != null)
            {
                OnAdsReady(placementId);
            }
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {

        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {

            // Reward the user for watching the ad to completion.
            if (SceneManager.GetActiveScene().name == "GameMenu")
            {
                GameManager.Instance.cellShop.AddItemToPlayerInventory(GameManager.Instance.cellShop.Item);
                GameManager.Instance.shopInventory.RemoveItemfromShopInventory();


            }
            else
            if (Player.Instance.Health.CurrentHealth <= 0)
            {
             //   if (Player.Instance.CountOfRevivals-Player.Instance.CurrentCountOfRevivals==0)
                Player.Instance.ContinueGame();
            }
            else
            {
                GameManager.Instance.playerInventory.GetReward();
            }

        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
        else
        {
            Debug.LogError("Error");
        }

    }

//  public void InitCellShop(CellShop cellShop)


    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }


}

