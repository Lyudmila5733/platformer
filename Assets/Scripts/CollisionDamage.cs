﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDamage : MonoBehaviour
{

    [SerializeField] private int damage;
    [SerializeField] private bool isBiting = false;
    //temp begin
    //    [SerializeField] private GameObject enemyArchering;

    //temp end
    public Animator animator;
    private Health health;
    private float direction;
    private GameObject colObject;
    public int Damage
    {
        get { return damage; }
        set { if (damage > 0.01) damage = value; }
    }
    public float Direction
    {
        get { return direction; }
    }
    public bool IsBiting
    {
        get { return isBiting; }
        set { isBiting = value; }
    }


    private void Start()
    {

    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        colObject = col.gameObject;
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject) && (animator != null))
        {
            if (!isBiting)
            {
                health = GameManager.Instance.healthContainer[col.gameObject];
                direction = (col.gameObject.transform.position - transform.position).x;
                isBiting = true;

                if (col.gameObject.CompareTag("Player"))
                {
                    Animator playerAnimator = col.gameObject.GetComponent<Animator>();
                    // playerAnimator.SetTrigger("StartDamage");
                    playerAnimator.SetBool("isBiting", isBiting);
                }
                animator.SetFloat("Direction", Mathf.Abs(direction));
            }
        }
    }

    public void SetDamage()
    {

        if (health != null)
        {
            health.TakeHit(damage);

            if (colObject.CompareTag("Player"))
            {
                GameManager.Instance.audioManager.PlayGetDamage();
               //   Debug.Log("colObject:" + colObject);
               Player player = FindObjectOfType<Player>();
                player.IsBlockMovement = true;
                int damageForce = player.DamageForce;
                player.rigidbody.AddForce(transform.position.x < player.transform.position.x ? new Vector2(damageForce, 0) : new Vector2(-damageForce, 0), ForceMode2D.Impulse);
            }
            isBiting = false;
            if (direction == 0)
                health = null;
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (health != null)
            if (col.gameObject == health.gameObject)
            {
                direction = 0;
                isBiting = false;

                if (col.gameObject.CompareTag("Player"))
                {
                    Animator playerAnimator = col.gameObject.GetComponent<Animator>();
                    playerAnimator.SetBool("isBiting", isBiting);
                }

                animator.SetFloat("Direction", 0f);
            }
    }

}

