﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;

[RequireComponent(typeof(Button))]
public class RewardedAdsButton : MonoBehaviour
{
    //  private CellShop cellShop;
    private BuffType type;
    Button myButton;
    static bool listenerSet = false;

    void Start()
    {
        myButton = GetComponent<Button>();

        // Set interactivity to be dependent on the Placement’s status:
        myButton.interactable = Advertisement.IsReady(GameManager.Instance.RewardedAdsComponent.myPlacementId);

        // Map the ShowRewardedVideo function to the button’s click listener:
        if (myButton)
        {
            //   Advertisement.RemoveListener(GameManager.Instance.RewardedAdsComponent);
            myButton.onClick.AddListener(ShowRewardedVideo);
        }
        GameManager.Instance.RewardedAdsComponent.OnAdsReady += OnUnityAdsReady;

    }

    // Implement a function for showing a rewarded video ad:
    void ShowRewardedVideo()
    {
//        Debug.Log("SHOW VIDEO!");
        if (SceneManager.GetActiveScene().name == "GameMenu")
            GameManager.Instance.InitCellShop(gameObject.transform.parent.transform.parent.transform.parent.GetComponent<CellShop>());

        Advertisement.Show(GameManager.Instance.RewardedAdsComponent.myPlacementId);

    }

    public void OnUnityAdsReady(string placementId)
    {
        myButton.interactable = true;
    }

}
