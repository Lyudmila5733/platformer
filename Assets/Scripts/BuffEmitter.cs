﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffEmitter : MonoBehaviour
{
    [SerializeField] private Buff buff;
    [SerializeField] private float periodBuffDamage = 1f;
    private bool isReadyBuffDamage = true;

    public Buff Buff_
    {
        get { return buff; }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.buffRecieverContainer.ContainsKey(col.gameObject))
        {
            var reciever = GameManager.Instance.buffRecieverContainer[col.gameObject];
            reciever.AddBuff(buff);
        }
    }

    public void OnTriggerStay2D(Collider2D col)
    {
                                                                
        if (col.gameObject.GetComponent<Rigidbody2D>() == null) return;
        Rigidbody2D rigidBody = col.gameObject.GetComponent<Rigidbody2D>();
        rigidBody.sleepMode = RigidbodySleepMode2D.NeverSleep;

        if (GameManager.Instance.buffRecieverContainer.ContainsKey(col.gameObject) && (isReadyBuffDamage))
        {
            var buffReciever = GameManager.Instance.buffRecieverContainer[col.gameObject];
            for (int i = 0; i < buffReciever.buffs.Count; i++)
            {
                if (buffReciever.buffs[i].type == BuffType.Health)
                {
                    var health = GameManager.Instance.healthContainer[col.gameObject];
                    health.TakeHit((int)buffReciever.buffs[i].additiveBonus);
                    Animator animator = Player.Instance.animator;
                    //animator.SetTrigger("StartDamage");
                    animator.SetBool("isBiting", true);
                    isReadyBuffDamage = false;
                    StartCoroutine(PeriodBuffDamage());
                }
            }
        }
        rigidBody.sleepMode = RigidbodySleepMode2D.StartAwake;
        
    }

    private IEnumerator PeriodBuffDamage()
    {
        yield return new WaitForSeconds(periodBuffDamage);
        isReadyBuffDamage = true;
        yield break;
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (GameManager.Instance.buffRecieverContainer.ContainsKey(col.gameObject))
        {
            var reciever = GameManager.Instance.buffRecieverContainer[col.gameObject];
            reciever.RemoveBuff(buff);
            Animator animator = Player.Instance.animator;
            animator.SetBool("isBiting", false);
        }
    }
}

[System.Serializable]
public class Buff
{
    public BuffType type;
    public float additiveBonus;
    public float multipleBonus;
}

public enum BuffType : byte
{

    Damage, Force, Armor, Health

}
