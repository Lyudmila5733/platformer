﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] Animator animator;
  //  [SerializeField] private int coin = 0;
    [SerializeField] private int coin;
    public bool isDestroying=false;

    private void Start()
    {
        GameManager.Instance.coinContainer.Add(gameObject, this);
    }

    public void StartDestroy()
    {
        isDestroying = true;
        animator.SetTrigger("StartDestroy");
    }

    public void EndDestroy()
    {
        Destroy(gameObject);
        isDestroying = false;
    }
}
