﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioClip shootSound;
    [SerializeField] private AudioClip jumpSound;
    [SerializeField] private AudioClip gameoverSound;
    [SerializeField] private AudioClip collectCoinSound;
    [SerializeField] private AudioClip collectChestSound;
    [SerializeField] private AudioClip collectBonusSound;
    [SerializeField] private AudioClip takeKeySound;
    [SerializeField] private AudioClip getDamageSound;
    [SerializeField] private AudioClip goToDoorSound;
    [SerializeField] private AudioClip clickButtonSound;
    [SerializeField] private AudioClip clickCancelSound;
    private Player player;
    void Start()
    {
        //        GetComponent<AudioSource>().volume = 0.2f;
        //       GetComponent<AudioSource>().PlayOneShot(backgroundSound);
        //       AudioClip = GetComponent<AudioSource>();
        //player = Player.Instance;
        //AudioSource = GetComponent<AudioSource>();
    }

    private void PlayClip(AudioClip clip)
    {
        if (PlayerPrefs.HasKey("Sound_On"))
        {
            if (PlayerPrefs.GetInt("Sound_On") == 1)
            {
                Player.Instance.GetComponent<AudioSource>().volume = 1f;
                Player.Instance.GetComponent<AudioSource>().PlayOneShot(clip);
            }
        }
    }

    public void PlayShoot()
    {
        PlayClip(shootSound);
    }

    public void PlayJump()
    {
        PlayClip(jumpSound);
    }

    public void PlayGameoverPanel()
    {
        PlayClip(gameoverSound);
    }

    public void PlayCollectCoin()
    {
        PlayClip(collectCoinSound);
    }

    public void PlayCollectChest()
    {
        PlayClip(collectChestSound);
    }

    public void PlayTakeKey()
    {
        PlayClip(takeKeySound);
    }

    public void PlayGetDamage()
    {
        PlayClip(getDamageSound);
    }

    public void PlayCollectBonus()
    {
        PlayClip(collectBonusSound);
    }

    public void PlayGoToDoor()
    {
        Debug.Log("goToDoorSound:"+ goToDoorSound);
        PlayClip(goToDoorSound);
    }
    public void PlayClickButton()
    {
        PlayClip(clickButtonSound);
    }
    public void PlayClickCancel()
    {
        PlayClip(clickCancelSound);
    }


}
