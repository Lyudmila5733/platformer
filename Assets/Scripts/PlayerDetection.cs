﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    [SerializeField] private bool isPlayerDetection = false;
    private float direction;
    public float Direction
    {
        get { return direction; }
    }
    public bool IsPlayerDetection
    {
        get { return isPlayerDetection; }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            isPlayerDetection = true;
            direction = (transform.parent.transform.position - col.gameObject.transform.position).x;
            return;
        }

    }

    private void OnTriggerExit2D(Collider2D col)
    {
        isPlayerDetection = false;
    }

}
