﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest_Key : MonoBehaviour
{
    [SerializeField] private GameObject keyPrize;
    public GameObject KeyPrize
    {
        get { return keyPrize; }
        set { keyPrize = value; }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;
        GameManager.Instance.audioManager.PlayCollectChest();
        if (keyPrize == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector2 position = new Vector2(transform.position.x, transform.position.y + 1.5f);
        keyPrize.transform.parent = gameObject.transform.parent;
        keyPrize.transform.position = position;
        Destroy(gameObject);
        keyPrize.SetActive(true);

    }

    private void Start()
    {
        if (GameManager.Instance.keyPrizeSymbol)
            GameManager.Instance.keyPrizeSymbol.SetActive(false);

    }



    /*
    private Transform SavePosition(GameObject gameObject)
    { 
        Vector2 position = new Vector2(transform.position.x, transform.position.y + 1);
        keyCreationPoint.parent = gameObject.transform.parent;
        keyCreationPoint.transform.position = position;
    
        return 
    }
*/
}
