﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicineChest : MonoBehaviour
{

    [SerializeField] private int bonusHealth;
    [SerializeField] private Animator animator;
    private bool isDestroying = false;

    public int BonusHealth
    {
        get { return bonusHealth; }
        set { if (bonusHealth > 0.01) bonusHealth = value; }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            Health health = GameManager.Instance.healthContainer[col.gameObject];
            if (health && !isDestroying)
            {
                health.AddHelth(bonusHealth);
                //Destroy(gameObject);
                StartDestroy();
            }
        }
    }

    public void StartDestroy()
    {
        isDestroying = true;
        animator.SetTrigger("StartDestroy");
    }

    public void EndDestroy()
    {
        Destroy(gameObject);
        isDestroying = false;
    }
}