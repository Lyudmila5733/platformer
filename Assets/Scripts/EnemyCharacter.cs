﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacter : MonoBehaviour
{
    [SerializeField] private PlayerDetection playerDetection;
    private void Update()
    {
        if (playerDetection.IsPlayerDetection)
            GetComponent<ArrowPool>().CheckShoot();
    }
}
