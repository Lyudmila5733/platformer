﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item Database", menuName = "Databases / Item")]
public class ItemBase : ScriptableObject
{
    [SerializeField, HideInInspector] private List<Item> items;
    [SerializeField] private Item currentItem;
    private int currentIndex;

    public void CreateItem()
    {
        if (items == null)
            items = new List<Item>();
        Item item = new Item();
        items.Add(item);
        currentItem = item;
        currentIndex = items.Count - 1;
    }

    public void RemoveItem()
    {
        if (items == null)
            return;
        if (currentItem == null)
            return;
        items.Remove(currentItem);
        if (items.Count > 0)
            currentItem = items[0];
        else CreateItem();
        currentIndex = 0;
    }

    public void NextItem()
    {
        if (items.Count>currentIndex+1)
        {
            currentIndex++;
            currentItem = items[currentIndex];
        }
    }

    public void PrevItem()
    {
        if (currentIndex > 0)
        {
            currentIndex--;
            currentItem = items[currentIndex];
        }
    }

    public Item GetItemOfId(int id)
    {
        return items.Find(t => t.ID == id);
    }

}

[System.Serializable]
public class Item
{
    [SerializeField] private int id;
    public int ID
    {
        get { return id; }
    }
    [SerializeField] private string itemName;
    public string Itemname
    {
        get { return itemName; }
    }

    [SerializeField] private string itemDescription;
    public string ItemDescription
    {
        get { return itemDescription; }
    }

    [SerializeField] private Sprite icon;
    public Sprite Icon
    {
        get { return icon; }
    }

    [SerializeField] private BuffType type;
    public BuffType Type
    {
        get { return type; }
    }
    [SerializeField] private float value;
    [SerializeField] private int cost;
    public float Value
    {
        get { return value; }
    }
    public int Cost
    {
        get { return cost; }
       // set { value = cost; }
    }

    public void Start()
    {
        //GameManager.Instance.itemContainer.Add(GameObject, this);
    }
}
