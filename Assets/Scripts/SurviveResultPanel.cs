﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurviveResultPanel : MonoBehaviour
{
    [SerializeField] private GameObject surviveResultsPanel; 
    [SerializeField] private Button continueButton;

    [SerializeField] private Text coinCount;
    [SerializeField] private Text amountOfWolf;
    [SerializeField] private Text amountOfRedWolf;
    [SerializeField] private Text amountOfShooter;

    // Start is called before the first frame update
    void Start()
    {
        surviveResultsPanel.SetActive(false);
        Debug.Log("Global.difficulityRandomLevel:" + Global.difficulityRandomLevel);
        if (Global.difficulityRandomLevel > 1)
        {
            surviveResultsPanel.SetActive(true);
            continueButton.onClick.AddListener(Continue);
        }


    }

    private void Continue()
    {
        surviveResultsPanel.SetActive(false);
    }

    // Update is called once per frame
    private void OnEnable()
    {
        //coinCount.text = "123";
        // coinCount.text = ""+ PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        coinCount.text = "" + PlayerPrefs.GetInt("LevelResultCoinCount");
        amountOfWolf.text = "" + Global.amountOfWolf;
        amountOfRedWolf.text = "" + Global.amountOfRedWolf;
        amountOfShooter.text = "" + Global.amountOfShooter;
    }
}
