﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    [SerializeField] private int health;
    [SerializeField] Transform prizeSpawnPoint;
    [SerializeField] private GameObject potionRoot;
    [SerializeField] private GameObject aidPrize;
    [SerializeField] private GameObject coinPrize;

    private int maxHealth;
    private ItemComponent currentItemComponent;
    private GameObject prize;

    private int amountOfWolf = 0;
    private int amountOfRedWolf = 0;
    private int amountOfShooter = 0;

    //   Transform prizeSpawnPoint;

    public int CurrentHealth
    {
        get { return health; }
        set
        {
            if (health > 0.01)
            {
                health = value;
            }
        }
    }
    public int MaxHealth
    {
        get { return maxHealth; }
        set { maxHealth = value; }
    }

    public void Start()
    {
        GameManager.Instance.healthContainer.Add(gameObject, this);
        Player player = FindObjectOfType<Player>();
        maxHealth = player.GetComponent<Health>().health;

        if (potionRoot != null)
            currentItemComponent = potionRoot.transform.GetChild(0).gameObject.GetComponent<ItemComponent>();

        Global.amountOfWolf = 0;
        Global.amountOfRedWolf = 0;
        Global.amountOfShooter = 0;
    }

    public void AddHelth(int bonusHealth)
    {
        health += bonusHealth;
        if (health > maxHealth)
            health = maxHealth;
    }
    public void SetHelth(int newHealth)
    {
        health += newHealth;
        if (health > maxHealth)
            health = maxHealth;
    }

    //    public void TakeHit(int damage, GameObject attacker)
    public void TakeHit(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            if (isPrizeGeneration())
            {
                Vector2 position = new Vector2(transform.position.x, transform.position.y + 1);
                prizeSpawnPoint.parent = gameObject.transform.parent;
                prizeSpawnPoint.transform.position = position;
                //               Debug.Log("prizeSpawnPoint.transform.position:"+ prizeSpawnPoint.transform.position);
            }

            if (CompareTag("Player"))
            {
                Player.Instance.OnDiedPlayer();
                /*
                if (Player.Instance.CountOfRevivals - Player.Instance.CurrentCountOfRevivals > 0)
                {        
                    Player.Instance.GetComponent<AudioSource>().Pause();
                    string yesText = "Yes (" + (Player.Instance.CountOfRevivals - Player.Instance.CurrentCountOfRevivals) + ")";
                    Player.Instance.GameoverPanel.GetComponent<GameOverPanelController>().Yes.gameObject.transform.GetChild(0).GetComponent<Text>().text = yesText;
                    Player.Instance.GameoverPanel.SetActive(true);
                    GameManager.Instance.audioManager.PlayGameoverPanel();

                    Debug.Log("Player.Instance.GetComponent<AudioSource> 1:" + Player.Instance.GetComponent<AudioSource>());
                }
                else Player.Instance.EndGame();
                */
            }
            else
            {
                if (CompareTag("Wolf")) Global.amountOfWolf++;
                if (CompareTag("RedWolf")) Global.amountOfRedWolf++;
                if (CompareTag("Shooter")) Global.amountOfShooter++;


                Destroy(gameObject);
                CheckPrizeGeneration();
            }

        }
    }

    private bool isPrizeGeneration()
    {
        if (gameObject.CompareTag("Player")) return false;
        if (prizeSpawnPoint == null) return false;
        return true;
    }

    private void CheckPrizeGeneration()
    {
        if (prizeSpawnPoint == null) return;
        if (aidPrize == null) return;
        if (potionRoot == null) return;
        GenerateRandomPrize();
        if (prize == null) return;
        Instantiate(prize, prizeSpawnPoint);
        //    StartCoroutine(generatePrize());
    }
    private void GenerateRandomPrize()
    {
        int randomNumber;
        randomNumber = Random.Range(1, 20);
        //  Debug.Log(randomNumber);
        //        if (randomNumber > 4) return;
        if (randomNumber >= 4)
        {
            prize = coinPrize.transform.GetChild(0).gameObject;
            return;
        }

        if (randomNumber == 4)
        {
            //            prize = aidPrize.transform.GetChild(0).gameObject;
            return;
        }
        if (randomNumber == 1) currentItemComponent.ItemType = ItemType.DamagePotion;
        if (randomNumber == 2) currentItemComponent.ItemType = ItemType.ArmorPotion;
        if (randomNumber == 3) currentItemComponent.ItemType = ItemType.ForcePoition;
        prize = currentItemComponent.gameObject;
    }

    private IEnumerator generatePrize()
    {
        Debug.Log("Start coroutine");
        yield return new WaitForSeconds(0);
        Debug.Log("Continue coroutine");
        Instantiate(currentItemComponent.gameObject, prizeSpawnPoint);
        yield break;
    }
}
