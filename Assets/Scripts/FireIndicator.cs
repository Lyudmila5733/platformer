﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireIndicator : MonoBehaviour
{
    [SerializeField] private Image fireImage;
    [SerializeField] private Image fireIconImage;
    [SerializeField] private float delta;
    [SerializeField] private Button fireButton;
    private float fireValue;
    private float currentFireValue;
    private Player player;
    private Color fireEnableColor;
    private Color fireDisableColor;
    void Start()
    {
        player = Player.Instance;
        fireValue = 1;
        delta = (float)1 / (player.ArrowRechargeTime * 120);
        currentFireValue = 1;
        fireEnableColor = new Color(1f, 1f, 1f, 0.85f);
        fireDisableColor = new Color(1f, 1f, 1f, 0.7f);

    }

    void FixedUpdate()
    {

        if (!player.IsReadyToShoot && currentFireValue == 1)
        {
            currentFireValue = 0;
            fireButton.interactable = false;
            fireIconImage.color = fireDisableColor;
        }

        if ((fireValue - currentFireValue) > 0)
            currentFireValue += delta;

        fireImage.fillAmount = currentFireValue;

        if (player.IsReadyToShoot)
        {
            currentFireValue = 1;
            fireButton.interactable = true;
            fireIconImage.color = fireEnableColor;
        }
    }
}