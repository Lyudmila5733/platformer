﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPanel : MonoBehaviour
{
    [SerializeField] private GameObject textPanel;
    [SerializeField] private Animator textAnimator;
    private Color transparentTextColor, textColor;

    private void Start()
    {
       transparentTextColor = new Color(0.4150943f, 0.1116055f, 0.1116055f, 0f);
       textColor = new Color(0.4150943f, 0.1116055f, 0.1116055f, 1f);
    }
    public void displaingText(string textOnDisplay)
    {

     //   textPanel.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        textPanel.transform.GetChild(0).GetComponent<Text>().color = transparentTextColor;
        textPanel.transform.GetChild(0).GetComponent<Text>().text = textOnDisplay;
        textAnimator.SetTrigger("ShowText");
        
    }

    private void Update()
    {
     //   displaingText("aaa");

    }
}
