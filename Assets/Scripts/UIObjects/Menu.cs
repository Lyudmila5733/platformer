﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private InputField nameField;
    [SerializeField] private GameObject panelLevels;
    [SerializeField] private GameObject panelSettings;
    [SerializeField] private GameObject panelBasic;
    [SerializeField] private GameObject panelLevelCompleted;
    [SerializeField] private GameObject panelShop;
    [SerializeField] private GameObject panelMessages;

    [SerializeField] private Button yesButton;
    [SerializeField] private Button noButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private Button soundButton;
    private int currentLevel;
    private int soundOn = 1;
    private string volumeOn = "Volume On";
    private string volumeOff = "Volume Off";

    public GameObject PanelLevelCompleted
    {
        get { return panelLevelCompleted; }
        set { panelLevelCompleted = value; }
    }
    public GameObject PanelLevels
    {
        get { return panelLevels; }
        set { panelLevels = value; }
    }
    public GameObject PanelBasic
    {
        get { return panelBasic; }
        set { panelBasic = value; }
    }
    public GameObject PanelShop
    {
        get { return panelShop; }
        set { panelShop = value; }
    }
    public Button YesButton
    {
        get { return yesButton; }
    }
    public Button NoButton
    {
        get { return noButton; }
    }
    public Button CloseButton
    {
        get { return closeButton; }
    }


    private void Start()
    {
        if (PlayerPrefs.HasKey("Player_Name") && !(nameField.text == null))
            nameField.text = PlayerPrefs.GetString("Player_Name");
        panelLevels.SetActive(false);
        panelSettings.SetActive(false);
        panelLevelCompleted.SetActive(false);
        panelShop.SetActive(false);
        if (panelBasic != null)
            panelBasic.SetActive(true);
        //       Debug.Log("Global.isDoorEntered:"+ Global.isDoorEntered);
        //       Debug.Log("Global.isSurviveLevel:" + Global.isSurviveLevel);
        if (Global.isDoorEntered && !Global.isSurviveLevel)
        {
            LevelCompletedPanelOpen();
        }

        Global.isDoorEntered = false;
        Global.isSurviveLevel = false;
        Global.difficulityRandomLevel = 1;

        //       Debug.Log("MENU START 1:" + PlayerPrefs.GetString("Player_Name") + "_currentLevel");
        //       Debug.Log("MENU START 2:" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") + "!!!");
        Debug.Log("1 PlayerPrefs.GetString(Player_Name):" + PlayerPrefs.GetString("Player_Name"));

        if (PlayerPrefs.GetString("Player_Name") == "") {
            Debug.Log("111");
            PlayerPrefs.SetString("Player_Name", "noname");
            PlayerPrefs.SetInt("Sound_On", 1);
        }
        Debug.Log("2 PlayerPrefs.GetString(Player_Name):"+ PlayerPrefs.GetString("Player_Name"));
        Debug.Log("PlayerPrefs.GetInt(Sound_On):" + PlayerPrefs.GetInt("Sound_On"));

        if (PlayerPrefs.HasKey("Sound_On"))
        {
            if (PlayerPrefs.GetInt("Sound_On") == 1)
            {
                SetTextButton(volumeOn);
                GameManager.Instance.GetComponent<AudioSource>().Play();
            }
            else SetTextButton(volumeOff);
        }

        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") == 0)
        {
            PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel", 1);
        }

        currentLevel = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel");


        //        Debug.Log("Nmuber of scene:" + SceneManager.GetSceneByName("Level_02").buildIndex);
        //        Debug.Log("Nmuber of scene:" + SceneManager.GetSceneByName("Level_03").buildIndex);
        //       Debug.Log("Nmuber of scene:" + SceneManager.GetSceneByName("GameMenu").buildIndex);


    }

    public void OnCliCkCompany()
    {
        panelLevels.SetActive(true);
        if (panelBasic != null)
            panelBasic.SetActive(false);
    }

    public void OnClickSettings()
    {
        Application.Quit();
    }

    public void OnEndEditName()
    {
//        Debug.Log("OnEndEditName!");
        PlayerPrefs.SetString("Player_Name", nameField.text);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void OnCliCkSurvive()
    {
        SceneManager.LoadScene(2);
        //   Debug.Log("SceneManager.GetSceneByName(\"SurviveLevel\").buildIndex:"+ SceneManager.GetSceneByName("SurviveLevel").buildIndex);
        //   SceneManager.LoadScene(SceneManager.GetSceneByName("SurviveLevel").buildIndex);
    }

    public void OnCliCkShop()
    {
        panelBasic.SetActive(false);
        panelSettings.SetActive(false);
        panelLevelCompleted.SetActive(false);
        panelLevels.SetActive(false);
        panelShop.SetActive(true);
    }

    public void OnClickSound()
    {
        

        if (PlayerPrefs.HasKey("Sound_On"))
        {
            if (PlayerPrefs.GetInt("Sound_On") == 0)
            {
                GameManager.Instance.GetComponent<AudioSource>().Play();
                SetTextButton(volumeOn);
                PlayerPrefs.SetInt("Sound_On", 1);
            }
            else if (PlayerPrefs.GetInt("Sound_On") == 1)
            {
                GameManager.Instance.GetComponent<AudioSource>().Stop();
                SetTextButton(volumeOff);
                PlayerPrefs.SetInt("Sound_On", 0);
            }
        }
    }
    private void SetTextButton(string text)
    {
        soundButton.GetComponentInChildren<Text>().text = text;
    }

    /*
        public void LoadScenaById(int id)
        {
            SceneManager.LoadScene(id);
        }
        */
    public void OnCliCkLevel01()
    {
        SceneManager.LoadScene(1);
    }
    public void OnCliCkLevel02()
    {
        SceneManager.LoadScene(2);
    }

    public void OnCliCkLevel03()
    {
        SceneManager.LoadScene(3);
    }
    public void OnCliCkLevel04()
    {
        SceneManager.LoadScene(4);
    }
    public void OnCliCkLevel05()
    {
        SceneManager.LoadScene(5);
    }
    public void OnCliCkGoButton()
    {
//        Debug.Log("Load Scene:" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel"));
        SceneManager.LoadScene(2 + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel"));
    }

    public void OnClickBackMainMenu()
    {
        panelLevels.SetActive(false);
        panelSettings.SetActive(false);
        panelLevelCompleted.SetActive(false);
        panelShop.SetActive(false);
        panelBasic.SetActive(true);

    }

    public void LevelCompletedPanelOpen()
    {
        panelLevels.SetActive(false);
        panelSettings.SetActive(false);
        panelLevelCompleted.SetActive(true);
        panelShop.SetActive(false);
        panelBasic.SetActive(false);
    }

    public void ShowMessagePanel()
    {
        panelMessages.SetActive(true);
    }


    /*
        public void OnClickYes()
        {
            Debug.Log("YES:" + GetComponent<CellShop>());
            panelMessages.SetActive(false);
         //   cellShop.OnClickCellAccepted();
        }

        public void OnClickNo()
        {

        }
        */

}
