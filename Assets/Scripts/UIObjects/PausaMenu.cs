﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PausaMenu : MonoBehaviour
{
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private GameObject inventoryPanel;
    [SerializeField] private GameObject gameoverPanel;
    [SerializeField] private Button soundButton;
    [SerializeField] private GameObject leftButton;
    [SerializeField] private GameObject rightButton;
    [SerializeField] private GameObject jumpButton;
    [SerializeField] private GameObject fireButton;
    [SerializeField] private Player player;
    private string volumeOn = "Volume On";
    private string volumeOff = "Volume Off";
    private int soundOn = 1;
    // private float timer;



    void Start()
    {
        menuPanel.SetActive(false);
        inventoryPanel.SetActive(false);
        gameoverPanel.SetActive(false);
        if (PlayerPrefs.HasKey("Sound_On"))
        {
            if (PlayerPrefs.GetInt("Sound_On") == 0)
            {
                SetTextButton(volumeOff);
            //    Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Stop();
            }
            else
            {
                SetTextButton(volumeOn);
                Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Play();
            }
        } else
        {
            SetTextButton(volumeOn);
            Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Play();
        }

        //   player.OnGameOver += showGameover;
    }


    private void Update()
    {


    }

    public void OnCliCkPause()
    {
        GameManager.Instance.audioManager.PlayClickButton();
        if (inventoryPanel.activeSelf == true) return;
        if (Time.timeScale > 0)
            Time.timeScale = 0;
        else Time.timeScale = 1;
        menuPanel.SetActive(true);
        changeActiveOfControlButtons(false);
    }

    public void OnClickExitToMenu()
    {
        GameManager.Instance.audioManager.PlayClickButton();
        SceneManager.LoadScene(1);

        if (Time.timeScale > 0)
            Time.timeScale = 0;
        else Time.timeScale = 1;

    }

    public void OnClickExit()
    {
        GameManager.Instance.audioManager.PlayClickCancel();
        Application.Quit();
    }

    public void OnClickContinueGame()
    {
        GameManager.Instance.audioManager.PlayClickButton();
        menuPanel.SetActive(false);
        if (Time.timeScale > 0)
            Time.timeScale = 0;
        else Time.timeScale = 1;
        changeActiveOfControlButtons(true);
    }

    public void OnClickInventory()
    {
        GameManager.Instance.audioManager.PlayClickButton();

        menuPanel.SetActive(false);
        inventoryPanel.SetActive(true);
        changeActiveOfControlButtons(false);
    }

    public void OnClickClose()
    {
        GameManager.Instance.audioManager.PlayClickCancel();
        inventoryPanel.SetActive(false);
        changeActiveOfControlButtons(true);
    }

    public void OnClickSound()
    {
        GameManager.Instance.audioManager.PlayClickButton();

        if (PlayerPrefs.HasKey("Sound_On"))
        {
            if (PlayerPrefs.GetInt("Sound_On") == 0)
            {
                Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Play();
                SetTextButton(volumeOn);
                PlayerPrefs.SetInt("Sound_On", 1);
            }
            else if (PlayerPrefs.GetInt("Sound_On") == 1)
            {
                Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Stop();
                SetTextButton(volumeOff);
                PlayerPrefs.SetInt("Sound_On", 0);
            }
        }

    }

    private void SetTextButton(string text)
    {
        soundButton.GetComponentInChildren<Text>().text = text;
    }

    private bool isSoundOn()
    {
        if (soundOn == 0) return false;
        if (soundOn == 1) return true;
        return false;
    }

    private void changeActiveOfControlButtons(bool setActive)
    {
        leftButton.SetActive(setActive);
        rightButton.SetActive(setActive);
        jumpButton.SetActive(setActive);
        fireButton.SetActive(setActive);
    }

    private void showGameover()
    {
        StartCoroutine(gameOver());
    }

    private IEnumerator gameOver()
    {
        yield return new WaitForSeconds(3);
        changeActiveOfControlButtons(false);
        Debug.Log("2:Continue coroutine");
        gameoverPanel.SetActive(true);
        yield break;
    }

}
