﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlatform : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    [SerializeField] private MovePlatform platform;
    [SerializeField] private Rigidbody2D rigidBody;
    private Vector3 direction;
    public GameObject upBorder;
    public GameObject downBorder;
    bool isUpNavigation = true;

    public MovePlatform Platform
    {
        get { return platform; }
        set { platform = value; }
    }

    private void Start()
    {
        if (platform.type == MovePlatformType.down_up)
            isUpNavigation = false;
    }
    void FixedUpdate()
    {
        Movement(platform.type);
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (platform.type != MovePlatformType.falling) return;
        if (rigidBody.velocity != new Vector2(0, 0)) return;
        StartCoroutine(Falling());
    }

    private void Movement(MovePlatformType type)
    {
        if (type == MovePlatformType.fixPosition) return;
        if (type == MovePlatformType.falling) return;
        if (type == MovePlatformType.up_down)
            MovingUpDownDownUp();

        if (type == MovePlatformType.down_up)
            MovingUpDownDownUp();

        if (type == MovePlatformType.up)
        {
            if (transform.position.y >= upBorder.transform.position.y)
            {
                rigidBody.velocity = new Vector2(0, 0);
                transform.position = new Vector2(downBorder.transform.position.x, downBorder.transform.position.y);
            }
            rigidBody.velocity = Vector2.up;
            rigidBody.velocity *= speed;
        }

        if (type == MovePlatformType.down)
        {
            if (transform.position.y <= downBorder.transform.position.y)
            {
                rigidBody.velocity = new Vector2(0, 0);
                transform.position = new Vector2(upBorder.transform.position.x, upBorder.transform.position.y);
            }
            rigidBody.velocity = Vector2.down;
            rigidBody.velocity *= speed;
        }
    }
    private void MovingUpDownDownUp()
    {
        if (transform.position.y >= upBorder.transform.position.y)
        {
            isUpNavigation = false;
            rigidBody.velocity = new Vector2(0, 0);
        }
        else if (transform.position.y <= downBorder.transform.position.y)
        {
            isUpNavigation = true;
            rigidBody.velocity = new Vector2(0, 0);
        }

        rigidBody.velocity = isUpNavigation ? Vector2.up : Vector2.down;
        rigidBody.velocity *= speed;
    }

    public void FallingPlatform()
    {
        int speed = 10;
        rigidBody.velocity = Vector2.down;
        rigidBody.velocity *= speed;
        StartCoroutine(TimeFalling(10));
    }

    private IEnumerator Falling()
    {
        rigidBody.velocity = new Vector2(0, 0.3f);
        yield return new WaitForSeconds(0.6f);
        rigidBody.velocity = Vector2.zero;
        yield return new WaitForSeconds(0.3f);
        FallingPlatform();
        yield break;
    }

    private IEnumerator TimeFalling(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        rigidBody.velocity = new Vector2(0, 0);
        transform.position = new Vector2(upBorder.transform.position.x, upBorder.transform.position.y + 100);
        yield break;
    }

}

[System.Serializable]
public class MovePlatform
{
    public MovePlatformType type;
}

public enum MovePlatformType
{
    up_down,
    down_up,
    up,
    down,
    fixPosition,
    falling
}

