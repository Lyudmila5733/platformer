﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInventory : MonoBehaviour
{

    [SerializeField] static private int coinCount;
    [SerializeField] private Text coinsText;
    [SerializeField] private Text healthText;
    [SerializeField] private Text damageText;
    [SerializeField] private Text forcejumpText;
    [SerializeField] private Text speedText;
    [SerializeField] private BuffReciever buffReciever;
    private List<Item> items;
    private int startCoinCount;
    private InventoryUIController controller;
    public BuffReciever BuffReciever
    {
        get { return buffReciever; }
    }
    public List<Item> Items
    {
        get { return items; }
    }
    public int CoinCount
    {
        get { return coinCount; }
        set { coinCount = value; }
    }
    public int StartCoinCount
    {
        get { return startCoinCount; }
    }
    public Text HealthText
    {
        get { return healthText; }
        set { healthText = value; }
    }
    public Text DamageText
    {
        get { return damageText; }
        set { damageText = value; }
    }
    public Text ForceJumpText
    {
        get { return forcejumpText; }
        set { forcejumpText = value; }
    }
    public Text SpeedText
    {
        get { return speedText; }
        set { speedText = value; }
    }

    private void Start()

    {
        GameManager.Instance.playerInventory = this;

        coinsText.text = "" + coinCount;

        //begin
        //      healthText.text = "" + Player.Instance.Health.CurrentHealth;
        //      damageText.text = "" + Player.Instance.ArrowPool[0].TriggerDamage.Damage;
        //      forcejumpText.text = "" + Player.Instance.JumpForce;
        //      speedText.text = "" + Player.Instance.Speed;
        //end


        items = new List<Item>();
        // GameManager.Instance.playerInventory.CoinCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        coinCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        startCoinCount = coinCount;
        coinsText.text = "" + coinCount;

        //   Debug.Log("PlayerPrefs ArmorPotionCount" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount"));
        //   Debug.Log("PlayerPrefs DamagePotionCount" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount"));
        //   Debug.Log("PlayerPrefs ForcePotionCount" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount"));



        for (int i = 0; i < PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount"); i++)
        {
            Item item = GameManager.Instance.itemDataBase.GetItemOfId(1);
            items.Add(item);

        }

        for (int i = 0; i < PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount"); i++)
        {
            Item item = GameManager.Instance.itemDataBase.GetItemOfId(2);
            items.Add(item);
        }

        for (int i = 0; i < PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount"); i++)
        {
            Item item = GameManager.Instance.itemDataBase.GetItemOfId(3);
            items.Add(item);
        }


        //       PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount", 0);
        //       PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount", 0);
        //       PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount", 0);

    }

    private void OnTriggerEnter2D(Collider2D col)
    {

        if (GameManager.Instance.coinContainer.ContainsKey(col.gameObject))
        {

            var coin = GameManager.Instance.coinContainer[col.gameObject];
            if (!coin.isDestroying)
            {
                coinCount++;
                GameManager.Instance.audioManager.PlayCollectCoin();
            }
            coinsText.text = "" + coinCount;
            PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount", coinCount);
            coin.StartDestroy();
        }

        if (GameManager.Instance.itemsContainer.ContainsKey(col.gameObject))
        {
            GameManager.Instance.audioManager.PlayCollectBonus();
            var itemComponent = GameManager.Instance.itemsContainer[col.gameObject];
            if (!itemComponent.isDestroying)
            {
                items.Add(itemComponent.Item);
                SaveItem(itemComponent.Item);
            }
            // itemComponent.Destroy(col.gameObject);
            itemComponent.StartDestroy();
        }
    }

    private void AddItemById(int id)
    {
        Item item = GameManager.Instance.itemDataBase.GetItemOfId(id);
        items.Add(item);
        SaveItem(item);
    }

    public void GetReward()
    {
        AddItemById(Random.Range(1, 4));
        FindObjectOfType<InventoryUIController>().UpdateInventory();

    }

    private void SaveItem(Item item)
    {
        if (item.Type == BuffType.Armor) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount") + 1);
        if (item.Type == BuffType.Damage) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount") + 1);
        if (item.Type == BuffType.Force) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount") + 1);
    }

    public void RemoveItemFromSaved(Item item)
    {
        if (item.Type == BuffType.Armor) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount") - 1);
        if (item.Type == BuffType.Damage) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount") - 1);
        if (item.Type == BuffType.Force) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount") - 1);

    }



}
