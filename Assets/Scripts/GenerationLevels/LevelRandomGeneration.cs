﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelRandomGeneration : MonoBehaviour
{
    [SerializeField] GameObject flowerPlatform;
    [SerializeField] GameObject flowerPlatformFlying;
    [SerializeField] GameObject redWolf;
    [SerializeField] GameObject wolf;
    [SerializeField] GameObject shooter;
    [SerializeField] GameObject movePlatform;
    [SerializeField] GameObject glass;
    [SerializeField] GameObject moss;
    [SerializeField] Transform parentRoot;
    [SerializeField] GameObject chest;
    [SerializeField] GameObject chestKey;
    [SerializeField] private int amountOfPlatforms;
    [SerializeField] private int amountOfFlyingPlatforms;
    [SerializeField] private int amountOfRedwolfs;
    [SerializeField] private int amountOfWolfs;
    [SerializeField] private int amountOfShooters;
    [SerializeField] private int amountOfChests;
    [SerializeField] private int amountOfGlasses;
    [SerializeField] private int amountOfMoss;
    [SerializeField] private float distanceXBetweenPlatforms=3f;

    [SerializeField] private Text survivelevelText;
    
    //private LevelsController levelsController;
    private int allPlatforms;
    private int maxHight=8;
    private int maxHightForNonflyingPlatforms=4;

    private Transform transform;
 //   private int x;
    private float x;
//    private int[] coordX;
    private float[] coordX;
//    private int y;
    private float y;
 //   private int[] coordY;
    private float[] coordY;
    private int counter = 0;
    private List<int> placingEnemies;
    private List<int> placingChests;
    private int maxY1=5;
    private int maxY2=10;


    private List<int[,]> listCoordinats;
    int tempCounter = 0;
    private bool isCoordinatsAccepted;

    private float[,] coord;


    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex==2)
        {

                placingEnemies = new List<int>();
                placingChests = new List<int>();
                coordX = new float[200];
                coordY = new float[200];

                InitDifficulity(Global.difficulityRandomLevel);
                          //InitDifficulity(4);

                survivelevelText.text = "Survive Level " + Global.difficulityRandomLevel;
                allPlatforms = amountOfPlatforms + amountOfFlyingPlatforms;
                CreateNonflyingPlatforms(flowerPlatform, amountOfPlatforms);
                CreateFlyingPlatforms(flowerPlatformFlying, amountOfFlyingPlatforms);
                GeneratedEnemies(redWolf, amountOfRedwolfs);
                GeneratedEnemies(wolf, amountOfWolfs);
                GeneratedEnemies(shooter, amountOfShooters);
                GeneratedEnemies(glass, amountOfGlasses);
                GeneratedEnemies(moss, amountOfMoss);
                MovingKeyChest(chestKey);
                GenerateChests(chest, amountOfChests);

        }
    }

    private void InitDifficulity(int difficulityLevel) {

        if (difficulityLevel == 1)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 0;
            amountOfShooters = 0;
            amountOfChests = 5;
            amountOfGlasses = 0;
            amountOfMoss = 0;
            distanceXBetweenPlatforms = 2.5f;
        }
        
        if (difficulityLevel == 2)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 3;
            amountOfShooters = 3;
            amountOfChests = 8;
            amountOfGlasses = 0;
            amountOfMoss = 0;
            distanceXBetweenPlatforms = 2.85f;
        }
        
        if (difficulityLevel == 3)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 3;
            amountOfShooters = 3;
            amountOfGlasses = 2;
            amountOfMoss = 2;
            amountOfChests = 10;
            distanceXBetweenPlatforms = 3.2f;
        }
        
        if (difficulityLevel == 4)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 4;
            amountOfShooters = 4;
            amountOfGlasses = 4;
            amountOfMoss = 4;
            amountOfChests = 10;
            distanceXBetweenPlatforms = 3.55f;
        }
        
        if (difficulityLevel == 5)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 6;
            amountOfShooters = 6;
            amountOfGlasses = 4;
            amountOfMoss = 4;
            amountOfChests = 12;
            distanceXBetweenPlatforms = 3.85f;
        }
        
        if (difficulityLevel == 6)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 6;
            amountOfShooters = 6;
            amountOfGlasses = 6;
            amountOfMoss = 6;
            amountOfChests = 20;
            distanceXBetweenPlatforms = 4.2f;
        }
        if (difficulityLevel > 6)
        {
            amountOfPlatforms = 50;
            amountOfFlyingPlatforms = 50;
            amountOfWolfs = 10;
            amountOfRedwolfs = 8;
            amountOfShooters = 8;
            amountOfGlasses = 6;
            amountOfMoss = 6;
            amountOfChests = 23;
            distanceXBetweenPlatforms = 4.55f;
        }

    }

    private Transform GenerateCoordinats(GameObject gameObject)
    {
        x = Random.Range(0, 80);
        y = Random.Range(0, 10);
        Vector2 position = new Vector2(x, y);
        gameObject.transform.position = position;
        return transform;
    }


    private Vector2 GenerateCoordinatsNew(int minY, int maxY)
    {

        //   int[] xmas = {10,5,16,18,14,0,11,6,2,13,5,11,3,18,18,4, 1,16};
        //  int[] xmas = {10,18,14, 0, 11,2};
        //       x = xmas[tempCounter];
        //        Debug.Log("tempCounter:" + tempCounter);
        //tempCounter++;

        isCoordinatsAccepted = true;

        x = Random.Range(10, 64f);
        y = Random.Range(1, maxY);
        coordX[counter] = x; coordY[counter] = y;

        for (int i = 0; i < counter; i++)
        {
            if (Mathf.Abs(y - coordY[i]) <= 1)
            {
                if (Mathf.Abs(x - coordX[i]) < distanceXBetweenPlatforms)
                {
                    counter--;
                    isCoordinatsAccepted = false;
                    allPlatforms--;
  //                  Debug.Log("BAD!!!");
                    //Debug.Log("amountOfFlyingPlatforms ("+i+"):" + amountOfFlyingPlatforms);
                    break;
                }
            }
        }

        counter++;
        Vector2 position = new Vector2(coordX[counter - 1], coordY[counter - 1]);
        return position;

    }
    private Vector2 GenerateCoordinatsNew2(float minY, float maxY)
    {

        //   int[] xmas = {10,5,16,18,14,0,11,6,2,13,5,11,3,18,18,4, 1,16};
        //  int[] xmas = {10,18,14, 0, 11,2};
        //       x = xmas[tempCounter];
        //        Debug.Log("tempCounter:" + tempCounter);
        //tempCounter++;

        isCoordinatsAccepted = true;

        x = Random.Range(10, 64f);
        y = Random.Range(minY+0.0001f, maxY+0.0001f);
        coordX[counter] = x; coordY[counter] = y;
//        Debug.Log(x + ";" + y);


        for (int i = 0; i < counter; i++)
        {
            if (Mathf.Sqrt((x-coordX[i])*(x - coordX[i]) + (y - coordY[i]) * (y - coordY[i])) < distanceXBetweenPlatforms)
                {
                    counter--;
                    isCoordinatsAccepted = false;
                    allPlatforms--;
 //                   Debug.Log("BAD!!!");
                    //Debug.Log("amountOfFlyingPlatforms ("+i+"):" + amountOfFlyingPlatforms);
                    break;
                }
            
        }

        counter++;
        Vector2 position = new Vector2(coordX[counter - 1], coordY[counter - 1]);
        return position;

    }

    private void GenerateChests(GameObject gameOject, int amount)
    {
        Debug.Log("Generated Platforms:" + allPlatforms);

        int currentPlacing;
        Vector2 coor;
        for (int i = 0; i < amount; i++)
        {
            currentPlacing = Random.Range(0, allPlatforms);
            //Debug.Log("currentPlacing:" + currentPlacing);
            if (!placingChests.Contains(currentPlacing))
            {
                placingChests.Add(currentPlacing);
                coor = new Vector2(coordX[currentPlacing], coordY[currentPlacing] + 0.25f);
                GameObject tempGameObject = Instantiate(gameOject, coor, Quaternion.identity, parentRoot);
                tempGameObject.transform.parent = parentRoot;
            }

        }
    }

    private void GeneratedEnemies(GameObject gameOject, int amount)
    {
  //      Debug.Log("allPlatforms:" + allPlatforms);

        int currentPlacing;
        Vector2 coor;
        for (int i = 0; i < amount; i++)
        {
            currentPlacing = Random.Range(0, allPlatforms);
 //           Debug.Log("currentPlacing:" + currentPlacing);
            if (!placingEnemies.Contains(currentPlacing))
            {
                placingEnemies.Add(currentPlacing);
                coor = new Vector2(coordX[currentPlacing], coordY[currentPlacing] + 0.3f);
 //               Debug.Log("enemy coors:" + coor);
                GameObject tempGameObject = Instantiate(gameOject, coor, Quaternion.identity, parentRoot);
                tempGameObject.transform.parent = parentRoot;
            }
 //           else Debug.Log("The same place!");

        }
    }

    private void MovingKeyChest(GameObject gameObject)
    {
        int currentPlacing;
        Vector2 coor;
        currentPlacing = Random.Range(0, allPlatforms);
 //       Debug.Log("currentPlacing 2:" + currentPlacing);
        placingChests.Add(currentPlacing);
        coor = new Vector2(coordX[currentPlacing], coordY[currentPlacing] + 0.25f);
        gameObject.transform.position = coor;
        gameObject.transform.parent = parentRoot;

    }
    


    private void CreateNonflyingPlatforms(GameObject gameOject, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector2 generatedCoordinats = GenerateCoordinatsNew2(0.75f, maxHightForNonflyingPlatforms);
            if (isCoordinatsAccepted)
            {
             //   Debug.Log("generatedCoordinats=" + generatedCoordinats);

                GameObject tempGameObject = Instantiate(gameOject, generatedCoordinats, Quaternion.identity, parentRoot);
                tempGameObject.transform.parent = parentRoot;
            }

        }
        //Debug.Log("amountOfFlyingPlatforms 2:" + amountOfFlyingPlatforms);
    }

    private void CreateFlyingPlatforms(GameObject gameOject, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector2 generatedCoordinats = GenerateCoordinatsNew2(maxHightForNonflyingPlatforms+0.01f, maxHight);
            if (isCoordinatsAccepted)
            {
             //   Debug.Log("generatedCoordinats=" + generatedCoordinats);

                GameObject tempGameObject = Instantiate(gameOject, generatedCoordinats, Quaternion.identity, parentRoot);
                tempGameObject.transform.parent = parentRoot;
            }

        }
        //Debug.Log("amountOfFlyingPlatforms 2:" + amountOfFlyingPlatforms);
    }




}
