﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.Concurrent;
using UnityEngine;

public class Level01 : MonoBehaviour
{
    private int platformsAmount = 8;
    private int movedPlatformsAmount = 2;
    private int wolfsAmount = 2;
    private int coinPacksAmount = 4;
    [SerializeField] private float chestKeyCoordX;
    [SerializeField] private float chestKeyCoordY;
    private LevelsController levelsController;
    private int x;
    private int y;
    private float[] platformsCoordX = { 10, 18, 22, 26, 30, 33.9f, 38, 42 };
    private float[] platformsCoordY = { 2, 2, 4, 6, 4, 8, 5.7f, 3f };
    private float[] wolfsCoordX = { 26, 44 };
    private float[] wolfsCoordY = { 0, 0 };
    private float[] coinPacksCoordX = { 10, 26, 34, 34 };
    private float[] coinPacksCoordY = { 3, 7, 9, 10 };
    private float[] movedPlatformsCoordX = { 52f, 60f };
    private float[] movedPlatformsCoordY = { 0, 0 };


    private int platformCounter = 0;

    private GameObject temp2;

    public void InitLevelsController(LevelsController controller)
    {
        levelsController = controller;
    }
    void Start()
    {

        CreatePlatforms(levelsController.FlowerPlatform, platformsAmount);
    //    CreateMovedPlatforms(levelsController.MovePlatform, movedPlatformsAmount);
        CreateWolfs(levelsController.Wolf, wolfsAmount);
        CreateCoinPacks(levelsController.CoinPack, coinPacksAmount);
        MovingKeyChest(levelsController.ChestKey);

    }


    private void CreatePlatforms(GameObject gameObject, int amount)
    {
        for (int i = 0; i < amount; i++)
                Instantiate(gameObject, new Vector2(platformsCoordX[i], platformsCoordY[i]), Quaternion.identity, levelsController.ParentGameobject);

    }
    private void CreateMovedPlatforms(GameObject gameObject, int amount)
    {
        for (int i = 0; i < amount; i++)
                Instantiate(gameObject, new Vector2(movedPlatformsCoordX[i], movedPlatformsCoordY[i]), Quaternion.identity, levelsController.ParentGameobject);

    }
    private void CreateWolfs(GameObject gameObject, int amount)
    {
        for (int i = 0; i < amount; i++)
                Instantiate(gameObject, new Vector2(wolfsCoordX[i], wolfsCoordY[i]), Quaternion.identity, levelsController.ParentGameobject);

    }
    private void CreateCoinPacks(GameObject gameObject, int amount)
    {
        for (int i = 0; i < amount; i++)
                Instantiate(gameObject, new Vector2(coinPacksCoordX[i], coinPacksCoordY[i]), Quaternion.identity, levelsController.ParentGameobject);

    }



    private void MovingKeyChest(GameObject gameObject)
    {
        Vector2 coor = new Vector2(platformsCoordX[6] + 0.88f, platformsCoordY[6] +0.38f);
        gameObject.transform.position = coor;
     //   gameObject.transform.parent = parentRoot;

    }

}
