﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Dictionary<GameObject, Health> healthContainer;
    public Dictionary<GameObject, Coin> coinContainer;
    public Dictionary<GameObject, BuffReciever> buffRecieverContainer;
    public Dictionary<GameObject, ItemComponent> itemsContainer;
    public ItemBase itemDataBase;
    public GameObject keyPrizeSymbol;
    public Door door;
    public CellShop cellShop;

    public RewardedAdsComponent RewardedAdsComponent;

    [HideInInspector] public PlayerInventory playerInventory;
    [HideInInspector] public ShopInventory shopInventory;
    [HideInInspector] public TriggerDamage triggerDamage;
    public AudioManager audioManager;
    //[SerializeField] private GameObject inventoryPanel;
    

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        healthContainer = new Dictionary<GameObject, Health>();
        coinContainer = new Dictionary<GameObject, Coin>();
        buffRecieverContainer = new Dictionary<GameObject, BuffReciever>();
        itemsContainer = new Dictionary<GameObject, ItemComponent>();

    }

    private void Start()
    {
 //       RewardedAdsComponent.removeItemfromShopInventory += shopInventory.RemoveItemfromShopInventory;

    }


    public void InitCellShop(CellShop cellShop)
    {
        this.cellShop = cellShop;

    }
}
