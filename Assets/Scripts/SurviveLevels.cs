﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SurviveLevels : MonoBehaviour
{
    [SerializeField] private GameObject surviveLevelsPavel;
    [SerializeField] private Button goButton;
    [SerializeField] private Button backButton;

    void Start()
    {
        surviveLevelsPavel.SetActive(true);
        goButton.onClick.AddListener(NextPlay);
        backButton.onClick.AddListener(BackToMainMenu);



        Time.timeScale = 0;

    }

    private void NextPlay()
    {
        surviveLevelsPavel.SetActive(false);
        Global.isSurviveLevel = true;
        Global.difficulityRandomLevel = Global.difficulityRandomLevel + 1;
        Time.timeScale = 1;

    }
    private void BackToMainMenu()
    {
        Global.difficulityRandomLevel = 1;
        Time.timeScale = 1;
        SceneManager.LoadScene(1);

    }


}
