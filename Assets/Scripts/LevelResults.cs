﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelResults : MonoBehaviour
{
    [SerializeField] private Text coinCount;
    [SerializeField] private Text amountOfWolf;
    [SerializeField] private Text amountOfRedWolf;
    [SerializeField] private Text amountOfShooter;
    [SerializeField] private Menu menuHome;
    private UIMenuController controller;
    private int tempCurrentLevel;

    #region Singleton
//    public static LevelResults Instance { get; set; }
    #endregion

    public void InitUIController(UIMenuController uicontroller)
    {
        controller = uicontroller;
        controller.GoButton.onClick.AddListener(OnClickGoButton);
        controller.BackButton.onClick.AddListener(OnClickBackButton);
        controller.ShopButton.onClick.AddListener(OnClickShopButton);
    }


    private void OnEnable()
    {
        //coinCount.text = "123";
       // coinCount.text = ""+ PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        coinCount.text = ""+ PlayerPrefs.GetInt("LevelResultCoinCount");
        amountOfWolf.text = "" + Global.amountOfWolf;
        amountOfRedWolf.text = "" + Global.amountOfRedWolf;
        amountOfShooter.text = "" + Global.amountOfShooter;
    }

    public void OnClickGoButton()
    {
       // Debug.Log("GO!");
       // menuHome.PanelLevelCompleted.SetActive(false);

        //    tempCurrentLevel = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel");
        //    PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel", tempCurrentLevel + 1);
        SceneManager.LoadScene(Global.tempCurrentLevel+2);
    }

    public void OnClickBackButton()
    {
//        Debug.Log("BACK!");
        menuHome.PanelLevelCompleted.SetActive(false);
 //       menuHome.PanelBasic.SetActive(true);
        menuHome.PanelLevels.SetActive(true);
        //SceneManager.LoadScene(0);

    }

    public void OnClickShopButton()
    {
        Debug.Log("SHOP!");
        menuHome.PanelLevelCompleted.SetActive(false);
        menuHome.PanelShop.SetActive(true);
    //    SceneManager.LoadScene(0);
    }

}
