﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShopInventory : MonoBehaviour
{
    [SerializeField] private ShopUIController controller;
    [SerializeField] private Text coinCount;

    private List<Item> items;
    public List<Item> Items
    {
        get { return items; }
    }

    public ShopUIController Controller
    {
        get { return controller; }
    }


    private void Awake()
    {
        Instance = this;
    }

    #region Singleton
    public static ShopInventory Instance { get; set; }
    #endregion

    private void Start()
    {
        //      Debug.Log("Start Shop inv");
        items = new List<Item>();
        GameManager.Instance.shopInventory = this;
        InitShopInventory();
        UpdateCoinCount();

        controller.updateCoinCountShopUI += UpdateCoinCount;
        GameManager.Instance.RewardedAdsComponent.removeItemfromShopInventory += RemoveItemfromShopInventory;

    }

    public void UpdateCoinCount() {
        coinCount.text = "" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
    }

    private void InitShopInventory()
    {
//        Debug.Log("Init shop potions");

        AddItemById(1);
        AddItemById(1);
        AddItemById(2);
        AddItemById(2);
        AddItemById(3);
        AddItemById(3);

 //       Debug.Log("items.Count:" + items.Count);

        controller.UpdateInventory();

    }

    private void AddItemById(int id)
    {
        Item item = GameManager.Instance.itemDataBase.GetItemOfId(id);
        items.Add(item);
    }

    public void RemoveItemfromShopInventory()
    {
//        Debug.Log("RemoveItemfromShopInventory!!!");
        GameManager.Instance.shopInventory.Items.Remove(GameManager.Instance.cellShop.Item);
        controller.UpdateInventory();

    }

    /*
    public void GetRewardByIdPotion(int id) {
        AddItemById(id);
    }
    */

}
