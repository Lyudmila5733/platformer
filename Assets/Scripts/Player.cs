﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour

{
    [SerializeField] private bool isCheatMode = true;
    [SerializeField] private float minimalHeight;
    [SerializeField] private bool isJumping;

    [SerializeField] private float speed = 3;
    [SerializeField] private Health health;             //armor
    [SerializeField] private HealthBar healthBar;             //armor
    [SerializeField] private float force = 5.9f;
    [SerializeField] private float jumpForce;        //force
    [SerializeField] private float shootForce = 4;

    [SerializeField] private Arrow arrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float arrowRechargeTime = 2f;
    [SerializeField] private bool isArrowCreating = false;
    [SerializeField] private int arrowsCount = 3;
    [SerializeField] private bool isReadyToShoot = true;

    [SerializeField] private BuffReciever buffReciever;
    [SerializeField] private bool isReadyBuffDamage = true;
    [SerializeField] private float periodBuffDamage = 1f;

    [SerializeField] private int damageForce;
    [SerializeField] private Camera playerCamera;

    [SerializeField] private GameObject gameoverPanel;
    [SerializeField] private int countOfRevivals;
    [HideInInspector] public bool isPossibleRuleByPlayer = true;
    private float damageBonus = 0;
    private float armorBonus = 0;
    private float forceBonus = 0;
    private TriggerDamage triggerDamage;
    public Rigidbody2D rigidbody;

    public GroundDetection groundDetection;
    private Vector3 direction;
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    private Arrow currentArrow;
    private List<Arrow> arrowPool;
    private float arrowDamage;
    private UICharacterController controller;
    private GameOverPanelController gameoverController;
    private bool isBlockMovement;
    private int currentCountOfRevivals;

    public Action OnGameOver;
    public float Speed
    {
        get { return speed; }
        set { if (speed > 0.01) speed = value; }
    }
    public float Force
    {
        get { return force; }
        set { if (force > 0.01) force = value; }
    }
    public float MinimalHeight
    {
        get { return minimalHeight; }
        set { if (force > 0.01) minimalHeight = value; }
    }
    public bool IsCheatMode
    {
        get { return isCheatMode; }
        set { isCheatMode = value; }
    }
    public Health Health
    {
        get { return health; }
    }
    public bool IsReadyToShoot
    {
        get { return isReadyToShoot; }
        set { isReadyToShoot = value; }
    }
    public bool IsReadyToDamage
    {
        get { return isReadyBuffDamage; }
        set { isReadyBuffDamage = value; }
    }
    public bool IsJumping
    {
        get { return isJumping; }
        set { isJumping = value; }
    }
    public float DamageBonus
    {
        get { return damageBonus; }
    }
    public float ArrowDamage
    {
        get { return arrowDamage; }
        set { arrowDamage = value; }
    }
    public bool IsArrowCreating
    {
        get { return isArrowCreating; }
    }
    public float ArrowRechargeTime
    {
        get { return arrowRechargeTime; }
    }
    public bool IsBlockMovement
    {
        get { return isBlockMovement; }
        set { isBlockMovement = value; }
    }
    public int DamageForce
    {
        get { return damageForce; }
    }
    public GameObject GameoverPanel
    {
        get { return gameoverPanel; }
    }
    public float JumpForce
    {
        get { return jumpForce; }
        set { jumpForce = value; }
    }
    public List<Arrow> ArrowPool
    {
        get { return arrowPool; }
    }
    public int CountOfRevivals
    {
        get { return countOfRevivals; }
    }
    public int CurrentCountOfRevivals
    {
        get { return currentCountOfRevivals; }
    }

    private void Awake()
    {
        Instance = this;
        isPossibleRuleByPlayer = true;
    }

    private void Start()
    {
        InitArrowPool();
        buffReciever.OnBuffsChanged += ApplyBuff;
        gameoverPanel.SetActive(false);
        // PlayerPrefs.SetInt("CoinCount", 0);
        currentCountOfRevivals = 0;

    }

    void FixedUpdate()
    {
        if (isPossibleRuleByPlayer)
        {
            Move();
            //    animator.SetFloat("Speed",rigidbody.velocity.x);
            animator.SetFloat("Speed", Mathf.Abs(direction.x));
        }
        CheckFall();

    }

    private void Update()
    {
        isPossibleRuleByPlayer = true;
        if (isPossibleRuleByPlayer)
        {
#if UNITY_EDITOR
            
            if (Input.GetKeyDown(KeyCode.Space))
                Jump();

#elif UNITY_STANDALONE_WIN
        if (Input.GetKeyDown(KeyCode.Space))
            Jump();
#endif
        }
    }
    public void Move()
    {
        animator.SetBool("isGrounded", groundDetection.IsGrounded);

        if (!isJumping && !groundDetection.IsGrounded) animator.SetTrigger("StartFall");
        isJumping = isJumping && !groundDetection.IsGrounded;

        direction = Vector3.zero;//(0,0)

#if UNITY_EDITOR
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left; //(-1, 0)

        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right; // (1, 0)
#elif UNITY_STANDALONE_WIN
        if (Input.GetKey(KeyCode.A))
            direction = Vector3.left; //(-1, 0)

        if (Input.GetKey(KeyCode.D))
            direction = Vector3.right; // (1, 0)
#endif

        if (controller.Left.IsPressed)
            direction = Vector3.left; //(-1, 0)

        if (controller.Right.IsPressed)
            direction = Vector3.right; // (1, 0)

        direction *= speed;
        direction.y = rigidbody.velocity.y;
        if (!isBlockMovement)
            rigidbody.velocity = direction;

        if (direction.x > 0) spriteRenderer.flipX = false;
        if (direction.x < 0) spriteRenderer.flipX = true;
    }
    public void UnBlockMovement()
    {
        isBlockMovement = false;
    }
    public void Jump()
    {
        if (groundDetection.IsGrounded)
        {
            GameManager.Instance.audioManager.PlayJump();

            if (isArrowCreating)
            {
                if (currentArrow.gameObject)
                    ReturnArrowToPool(currentArrow);
            }
            rigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            animator.SetTrigger("StartJump");
            isJumping = true;
            isReadyToShoot = true;
            //    isReadyToShoot = false;
        }
    }

    #region Singleton
    public static Player Instance { get; set; }
    #endregion

    public void InitUIController(UICharacterController uicontroller)
    {
        controller = uicontroller;
        controller.Jump.onClick.AddListener(Jump);
        controller.Fire.onClick.AddListener(CheckShoot);
    }
    public void InitGameoverController(GameOverPanelController gocontroller)
    {
        gameoverController = gocontroller;
        //      gameoverController.Yes.onClick.AddListener(ContinueGame);
        gameoverController.No.onClick.AddListener(EndGame);

    }

    private void ApplyBuff()
    {
        List<Buff> currentBuffs = GameManager.Instance.buffRecieverContainer[gameObject].buffs;
        //Debug.Log("START: currentBuffs.Count:" + currentBuffs.Count);
        for (int i = 0; i < currentBuffs.Count; i++)
        {
            //           Debug.Log("currentBuffs[" + i + "].type:" + currentBuffs[i].type);

            if (currentBuffs[i].type == BuffType.Damage)
            {
                damageBonus = currentBuffs[i].additiveBonus;
                InitArrowPool();
            }

            if (currentBuffs[i].type == BuffType.Armor)
                armorBonus = currentBuffs[i].additiveBonus;

            if (currentBuffs[i].type == BuffType.Force)
                forceBonus = currentBuffs[i].additiveBonus;
        }

        health.MaxHealth += (int)armorBonus;
        healthBar.MaxHealth = health.MaxHealth;
        health.AddHelth((int)armorBonus);
        jumpForce += forceBonus;

        for (int i = 0; i < currentBuffs.Count; i++)
        {
            if (currentBuffs[i].type == BuffType.Damage || currentBuffs[i].type == BuffType.Armor || currentBuffs[i].type == BuffType.Force)
                currentBuffs.Remove(currentBuffs[i]);
        }

        forceBonus = 0;
        armorBonus = 0;

    }

    private void InitArrowPool()
    {
        if (arrowPool != null)
            for (int i = 0; i <= arrowsCount - 1; i++)
                arrowPool[i].TriggerDamage.Damage += (int)damageBonus;
        else
        {
            arrowPool = new List<Arrow>();
            for (int i = 1; i <= arrowsCount; i++)
            {
                Arrow tempArrow = Instantiate(arrow, arrowSpawnPoint);
                tempArrow.gameObject.SetActive(false);
                tempArrow.TriggerDamage.Damage += (int)damageBonus;
                arrowPool.Add(tempArrow);
                if (currentArrow == null) currentArrow = tempArrow;
            }
        }

    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var tempArrow = arrowPool[0];
            arrowPool.Remove(tempArrow);
            tempArrow.gameObject.SetActive(true);
            tempArrow.transform.parent = null;
            tempArrow.transform.position = arrowSpawnPoint.transform.position;
            return tempArrow;
        }
        return Instantiate(arrow, arrowSpawnPoint.position, spriteRenderer.flipX ? Quaternion.Euler(0, 180, 0) : Quaternion.identity);
    }

    public void ReturnArrowToPool(Arrow tempArrow)
    {
        if (!arrowPool.Contains(tempArrow))
        {
            arrowPool.Add(tempArrow);
            tempArrow.transform.parent = arrowSpawnPoint;
            tempArrow.transform.position = arrowSpawnPoint.transform.position;
            tempArrow.gameObject.SetActive(false);
        }

    }
    void CheckShoot()
    {
        //    if (!groundDetection.IsGrounded) return;
        if (isPossibleRuleByPlayer)
        {
            if (isReadyToShoot)
            {
                isArrowCreating = true;
                animator.SetTrigger("StartArchery");
                isReadyToShoot = false;
                GameManager.Instance.audioManager.PlayShoot();
            }
        }
    }

    void СreateArrow()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.GetComponent<Rigidbody2D>().isKinematic = true;
        currentArrow.transform.parent = gameObject.transform;
        if (spriteRenderer.flipX) currentArrow.transform.rotation = Quaternion.Euler(0, 180, 0); else currentArrow.transform.rotation = Quaternion.identity;
        currentArrow.SetImpulseFromPlayer(Vector2.right, 0, this);
    }

    void Shoot()
    {
        currentArrow.GetComponent<Rigidbody2D>().isKinematic = false; //trouble
        currentArrow.transform.parent = null;
        currentArrow.SetImpulseFromPlayer(Vector2.right, spriteRenderer.flipX ? -force * shootForce : force * shootForce, this);
        isArrowCreating = false;
        StartCoroutine(RechargeTime());
    }

    private void checkArrowParent()
    {


        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == "Arrow")
            {
                //      ReturnArrowToPool(transform.GetChild(i));
            }
        }

    }

    void CheckPeriodBuffDamage() //depricated
    {
        if (GameManager.Instance.buffRecieverContainer.ContainsKey(gameObject) && (isReadyBuffDamage))
        {
            BuffReciever buffReciever = GameManager.Instance.buffRecieverContainer[gameObject];
            if (buffReciever.buffs.Count != 0)
            {
                for (int i = 0; i < buffReciever.buffs.Count; i++)
                {
                    if (buffReciever.buffs[i].type == BuffType.Health)
                    {
                        var health = GameManager.Instance.healthContainer[gameObject];
                        health.TakeHit((int)buffReciever.buffs[i].additiveBonus);
                        animator.SetTrigger("StartDamage");
                        isReadyBuffDamage = false;
                        StartCoroutine(PeriodBuffDamage());
                    }
                }
            }
        }
    }


    private IEnumerator PeriodBuffDamage() //depricated 
    {
        yield return new WaitForSeconds(periodBuffDamage);
        isReadyBuffDamage = true;
        yield break;
    }

    private IEnumerator RechargeTime()
    {
        yield return new WaitForSeconds(arrowRechargeTime);
        isReadyToShoot = true;
        yield break;
    }


    private void CheckFall()
    {
//        Debug.Log("transform.position.y < minimalHeight:" + (transform.position.y < minimalHeight));
//        Debug.Log("isCheatMode:" + (isCheatMode));
        if ((transform.position.y < minimalHeight) && (isCheatMode))
        {
            rigidbody.velocity = new Vector2(0, 0);
            transform.position = new Vector2(0, 0);
        }
        else
            if ((transform.position.y < minimalHeight) && (!isCheatMode))
        {
            health.TakeHit(health.CurrentHealth);
            Debug.Log(health.CurrentHealth);
            OnDiedPlayer();
        }
        //Destroy(gameObject);
    }

    public void ContinueGame()
    {
        currentCountOfRevivals++;
        health.SetHelth(100);
        rigidbody.velocity = new Vector2(0, 0);
        if (!Global.isFalledInWater)
            //   transform.position = new Vector2(transform.position.x-3f, transform.position.y+1f);
            transform.position = new Vector2(0, 0);
        else
        {
            Global.isFalledInWater = false;
            transform.position = new Vector2(0, 0);
        }
        gameoverPanel.SetActive(false);
        //        GetComponent<AudioSource>().Play();
        Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Play();


    }

    public void EndGame()
    {
        //   Destroy(gameObject);
        Debug.Log("GameManager.Instance.playerInventory.CoinCount:" + GameManager.Instance.playerInventory.CoinCount);
        Debug.Log("GameManager.Instance.playerInventory.StartCoinCount:" + GameManager.Instance.playerInventory.StartCoinCount);
        PlayerPrefs.SetInt("LevelResultCoinCount", GameManager.Instance.playerInventory.CoinCount - GameManager.Instance.playerInventory.StartCoinCount);
        Global.isDoorEntered = true;
        SceneManager.LoadScene(1);

    }

    public void OnDiedPlayer()
    {
        if (CountOfRevivals - CurrentCountOfRevivals > 0)
        {
            //GetComponent<AudioSource>().Stop();
            Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Stop();
            string yesText = "Yes (" + (CountOfRevivals - CurrentCountOfRevivals) + ")";
            GameoverPanel.GetComponent<GameOverPanelController>().Yes.gameObject.transform.GetChild(0).GetComponent<Text>().text = yesText;
            GameoverPanel.SetActive(true);
            GameManager.Instance.audioManager.PlayGameoverPanel();
            //            Debug.Log("Player.Instance.GetComponent<AudioSource> 1:" + Player.Instance.GetComponent<AudioSource>());
        }
        else EndGame();
    }

    private void OnDestroy()
    {

        playerCamera.transform.parent = null;
        playerCamera.enabled = true;
        gameoverPanel.SetActive(false);

        if (Health.CurrentHealth > 0) return;
        Debug.Log("Game over!");
        Debug.Log("Health.CurrentHealth:" + Health.CurrentHealth);

        /*
        if (OnGameOver != null)
            OnGameOver();
          */
    }


}




