﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    private float gravityPlayer;
    private Rigidbody2D playerRigidbody;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;

    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;

        playerRigidbody = col.gameObject.GetComponent<Rigidbody2D>();
        playerRigidbody.sleepMode=RigidbodySleepMode2D.NeverSleep;
        if (playerRigidbody.gravityScale!=0)
            gravityPlayer = playerRigidbody.gravityScale;



        playerRigidbody.gravityScale = 0;
        Debug.Log("gravityPlayer==" + playerRigidbody.gravityScale);

        if (Input.GetKeyDown(KeyCode.W)) { 
        Debug.Log(" W pressed");
            //playerRigidbody.velocity = new Vector2(0, 2);
            playerRigidbody.velocity = Vector3.up*3;

        }
        if (Input.GetKeyUp(KeyCode.W)) { 
            //playerRigidbody.velocity = new Vector2(0, 2);
            playerRigidbody.velocity = Vector3.zero;

        }
        if (Input.GetKeyDown(KeyCode.S)) { 
        Debug.Log(" S pressed");
            //playerRigidbody.velocity = new Vector2(0, 2);
            playerRigidbody.velocity = Vector3.down*3;

        }
        if (Input.GetKeyUp(KeyCode.S)) { 
            playerRigidbody.velocity = Vector3.zero;

        }
        Debug.Log("playerRigidbody.velocity="+ playerRigidbody.velocity);

    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;
        col.gameObject.GetComponent<Rigidbody2D>().gravityScale = gravityPlayer;
        playerRigidbody.sleepMode = RigidbodySleepMode2D.NeverSleep;
    }

}
