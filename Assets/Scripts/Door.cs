﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Door : MonoBehaviour
{
    [SerializeField] private SpriteRenderer openedDoorSprite;
    [SerializeField] private bool isMediumDoor = true;
    [SerializeField] private bool isSurviveDoor = false;
    [SerializeField] private Vector2 newPosition;
    [SerializeField] private TextPanel textPanel;
    //   [SerializeField] private GameObject levelCompletedPanel;

    private bool isOpened = false;
    private GameObject keyPrize;
    private GameObject player;
    private int ForcePotionCount = 0;
    private int ArmorPotionCount = 0;
    private int DamagePotionCount = 0;
    private Item item;
    private InventoryUIController controller;
    private int tempCurrentLevel;

    public void SetOpenedDoorSprite()
    {
        GetComponent<SpriteRenderer>().sprite = openedDoorSprite.sprite;
        isOpened = true;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;

        if (isOpened)
        {
            Debug.Log("GameManager.Instance.audioManager:"+ GameManager.Instance.audioManager);
            GameManager.Instance.audioManager.PlayGoToDoor();
            Player.Instance.transform.GetChild(0).transform.GetChild(0).GetComponent<AudioSource>().Stop();
            Player.Instance.isPossibleRuleByPlayer = false;
            //Player.Instance.groundDetection.IsGrounded = true;
           // Player.Instance.rigidbody.velocity = new Vector2(1.5f, Player.Instance.transform.position.y);
            if (isMediumDoor)
            {
                player = col.gameObject;
                // textPanel.displaingText("Level Continue");

                StartCoroutine(NewPartOfScenaLoadedTime());

            }

            else
            {
                //                textPanel.displaingText("Level Completed");
                //            Debug.Log("1...");
                if (GameManager.Instance == null)
                    Debug.Log("Instance NULL");
                if (GameManager.Instance.playerInventory == null)
                    Debug.Log("playerInventory NULL");
                if (GameManager.Instance.playerInventory.CoinCount == null)
                    Debug.Log("CoinCount NULL");
                //             Debug.Log("GameManager.Instance.playerInventory.CoinCount:" + GameManager.Instance.playerInventory.CoinCount);
                //             Debug.Log("GameManager.Instance.playerInventory.StartCoinCount:" + GameManager.Instance.playerInventory.StartCoinCount);

                PlayerPrefs.SetInt("LevelResultCoinCount", GameManager.Instance.playerInventory.CoinCount - GameManager.Instance.playerInventory.StartCoinCount);

                StartCoroutine(NewScenaLoadedTime());

                //  GameManager.Instance.playerInventory.CoinCount = PlayerPrefs.GetInt("CoinCount");
                //  Debug.Log("222 CoinCount:" + GameManager.Instance.playerInventory.CoinCount);

            }
        }
    }

    private IEnumerator NewScenaLoadedTime()
    {

        yield return new WaitForSeconds(1f);
        Global.isDoorEntered = true;
        Debug.Log("isSurviveDoor:" + isSurviveDoor);
        if (isSurviveDoor)
        {
            //            SceneManager.LoadScene(2);
            SceneManager.LoadScene(SceneManager.GetSceneByName("SurviveLevel").buildIndex);
        }
        else
        {
            tempCurrentLevel = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel");
            if (SceneManager.GetActiveScene().buildIndex == tempCurrentLevel + 2)
            {
                PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel", tempCurrentLevel + 1);
            }
            Global.tempCurrentLevel = Global.tempCurrentLevel + 1;

            //Debug.Log("Global.tempCurrentLevel:" + Global.tempCurrentLevel);

            SceneManager.LoadScene(1);
        }
        yield break;
    }

    private IEnumerator NewPartOfScenaLoadedTime()
    {
        yield return new WaitForSeconds(1f);
        player.transform.position = newPosition;
        keyPrize = GameManager.Instance.keyPrizeSymbol;
        keyPrize.SetActive(false);
        yield break;
    }



}
