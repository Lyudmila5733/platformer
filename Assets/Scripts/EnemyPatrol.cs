﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{

    [SerializeField] private float speed;
    public float Speed
    {
        get { return speed; }
        set { if (speed > 0.01) speed = value; }
    }
    public GameObject leftBorder;
    public GameObject rightBorder;
    public Rigidbody2D rigidBody;
    public GroundDetection groundDetection;
    public SpriteRenderer spriteRenderer;

    bool isRightNavigation = true;
   // private Vector3 direction;
    [SerializeField] private CollisionDamage collisionDamage;


    private void Update()
    {
        if (groundDetection.IsGrounded)
        {
            if (transform.position.x >= rightBorder.transform.position.x||collisionDamage.Direction < 0) isRightNavigation = false;
            else if (transform.position.x <= leftBorder.transform.position.x || collisionDamage.Direction > 0) isRightNavigation = true;

            rigidBody.velocity = isRightNavigation ? Vector2.right : Vector2.left;
            rigidBody.velocity *= speed;
        }


        if (rigidBody.velocity.x >= 0) spriteRenderer.flipX = true;
        if (rigidBody.velocity.x < 0) spriteRenderer.flipX = false;

    }
}


