﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    [SerializeField] private bool isGrounded;
    //    [SerializeField] private LayerMask platformLayerMask;
    private float distanceX = 0.25f;
    private float distanceWolfX = 0.53f;
    private float distanceY = 0.2f;
    private float distanceEnemyY = 0.5f;
    private float distanceYUp = 0.3f;
    public bool IsGrounded
    {
        get { return isGrounded; }
        set { isGrounded = value; }
    }
    private void Update()
    {
        if (CompareTag("Player"))
            CheckPlayerIsGrounded();
        else if (CompareTag("Shooter")) { }// CheckEnemyIsGrounded(distanceX);
        else CheckEnemyIsGrounded(distanceWolfX);
    }
    public void CheckPlayerIsGrounded()
    {
        var raycastHits = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x - distanceX, transform.position.y), Vector2.down, distanceY));
        var raycastHitsRight = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x + distanceX, transform.position.y), Vector2.down, distanceY));
        raycastHits.AddRange(raycastHitsRight);

        var raycastHitsUp = Physics2D.RaycastAll(transform.position, Vector2.up, distanceYUp);
        if (raycastHits != null)
        {
            foreach (var rh in raycastHits)
            {
                Debug.DrawRay(new Vector2(transform.position.x - distanceX, transform.position.y), Vector2.up * 1.0f, Color.red);
                Debug.DrawRay(new Vector2(transform.position.x + distanceX, transform.position.y), Vector2.up * 1.0f, Color.red);
                Debug.DrawRay(new Vector2(transform.position.x - distanceX, transform.position.y), Vector2.down * (distanceY), Color.green);
                Debug.DrawRay(new Vector2(transform.position.x + distanceX, transform.position.y), Vector2.down * (distanceY), Color.green);
                if (rh.transform.CompareTag("Platform"))
                {
                    if (raycastHitsUp != null)
                    {
                        foreach (var rhu in raycastHitsUp)
                        {
                            if (rhu.transform.CompareTag("Platform"))
                            {
                                IsGrounded = false;
                                return;
                            }
                        }
                    }
                    IsGrounded = true;
                    Debug.Log("PLATFORM! - " + isGrounded);
                    return;
                }
            }
        }
        isGrounded = false;
    }

    private void CheckEnemyIsGrounded(float distX)
    {
        var raycastHits = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x - distX, transform.position.y), Vector2.down, distanceEnemyY));
        var raycastHitsRight = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x + distX, transform.position.y), Vector2.down, distanceEnemyY));
        raycastHits.AddRange(raycastHitsRight);

        if (raycastHits != null)
        {
            foreach (var rh in raycastHits)
            {
                Debug.DrawRay(new Vector2(transform.position.x - distX, transform.position.y), Vector2.down * (distanceY), Color.green);
                Debug.DrawRay(new Vector2(transform.position.x + distX, transform.position.y), Vector2.down * (distanceY), Color.green);
                if (rh.transform.CompareTag("Platform"))
                {
                    IsGrounded = true;
                    return;
                }
            }
            IsGrounded = false;
            return;
        }
    }

    /*  
  public bool CheckIsGrounded()
  {
     // RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size, 0f, Vector2.down, 0.5f, platformLayerMask);
              RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, Vector2.down, 0.5f, platformLayerMask);

      Debug.Log("raycastHit.collider:" + raycastHit.collider);

      Color rayColor;
      if (raycastHit.collider != null)
      {
          rayColor = Color.green;
      }
      else rayColor = Color.red;
      Debug.DrawRay(transform.position, Vector2.down * (0.5f), rayColor);
      isGrounded = raycastHit.collider != null;

      Debug.Log("isGrounded 1:" + isGrounded);
      return isGrounded;
  }

      */




     
         public void OnCollisionStay2D(Collision2D col)
         {
            if (CompareTag("Shooter"))
             if (col.gameObject.CompareTag("Platform")|| col.gameObject.CompareTag("Wolf")|| col.gameObject.CompareTag("RedWolf")||col.gameObject.CompareTag("Shooter"))
             {
                 isGrounded = true;
                 return;
             }

         }


        public void OnCollisionExit2D(Collision2D col)
         {
            if (CompareTag("Shooter"))
            if (col.gameObject.CompareTag("Platform") || col.gameObject.CompareTag("Wolf") || col.gameObject.CompareTag("RedWolf") || col.gameObject.CompareTag("Shooter"))
             {
                 isGrounded = false;

             }

         }
      

}
