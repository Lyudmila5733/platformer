﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Cell : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private Text numberItems;
 //   [SerializeField] private SpriteRenderer emptyCellImage;
    private Color cellBackground;
    public Action updateUIInventory;
    private Item item;
    private Dictionary<BuffType, int> itemRepeatedContainer;
    public Image Icon
    {
        get { return icon; }
        set { icon = value; }
    }
    public Color CellBackground
    {
        get { return cellBackground; }
    }
    public Text NumberItems
    {
        get { return numberItems; }
        set { numberItems = value; }
    }
    private void Awake()
    {
        //   cellBackground = new Color(0.3521271f, 0.3582445f, 0.3867925f, 1);
        cellBackground = Color.black;
        icon.sprite = null;
        icon.color = cellBackground;
        numberItems.text = null;
        /*
        itemRepeatedContainer = new Dictionary<BuffType, int>();
        itemRepeatedContainer.Add(BuffType.Armor, 0);
        itemRepeatedContainer.Add(BuffType.Force, 0);
        itemRepeatedContainer.Add(BuffType.Damage, 0);
        */
    }

    public void Init(Item item)
    {

        this.item = item;
        if (item == null)
        {
            icon.sprite = null;
            icon.color = cellBackground;
            numberItems.text = null;
        }
        else
        {
            icon.sprite = item.Icon;
            icon.color = Color.white;
        }
    }
    /*
    public void InitUpgrade(Item item, int amountOfItems)
    {
        this.item = item;
        if (item == null)
        {
            icon.sprite = null;
            icon.color = cellBackground;
        }
        else
        {
            Debug.Log("");
            Debug.Log("");
            itemRepeatedContainer.Add(BuffType.Armor, 0);


        }
    }
    */

    public void OnClickCell()
    {
        if (item == null)
            return;
        GameManager.Instance.playerInventory.Items.Remove(item);
        GameManager.Instance.playerInventory.RemoveItemFromSaved(item);

        Buff buff = new Buff
        {
            type = item.Type,
            additiveBonus = item.Value
        };

        GameManager.Instance.playerInventory.BuffReciever.AddBuff(buff);

        if (updateUIInventory != null)
        {
            updateUIInventory();
        }
    }
}
