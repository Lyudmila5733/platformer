﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{

    [SerializeField] private int damage = 10;
    public int Damage_
    {
        get { return damage; }
        set
        {
            if (damage > 0.01)
            {
                damage = value;
            }
        }
    }

}
