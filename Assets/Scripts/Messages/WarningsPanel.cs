﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningsPanel : MonoBehaviour
{

    [SerializeField] private Text textMessage;
    [SerializeField] private Menu menu;
    private string noCoinsWarning;

    void Start()
    {
        noCoinsWarning = "No enough coins";
        textMessage.text = noCoinsWarning;
        gameObject.SetActive(false);
    }



    private void OnEnable()
    {
        menu.CloseButton.onClick.AddListener(OnClickClose);
    }

    private void OnDisable()
    {

    }


    public void OnClickClose()
    {
        Debug.Log("Close");
        gameObject.SetActive(false);
    }

}
