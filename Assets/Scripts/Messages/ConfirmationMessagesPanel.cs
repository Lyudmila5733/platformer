﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ConfirmationMessagesPanel : MonoBehaviour
{

    [SerializeField] private Text textMessage;
    [SerializeField] private CellShop cellShop;
    [SerializeField] private Menu menu;
    [SerializeField] private ShopUIController controller;
    [SerializeField] private Button closeButton;
 //   [SerializeField] private GameObject menuButtons;
    private bool isConfirmation = false;
    private Item item;
    public bool IsConfirmation
    {
        get { return isConfirmation; }
    }

    private string purchaseСonfirmation;
    //private string noCoinsWarning;
    void Start()
    {
        purchaseСonfirmation = "Do you sure you want buy this item?";
        //noCoinsWarning = "No enough coins";
        textMessage.text = purchaseСonfirmation;
        gameObject.SetActive(false);

    }

    public void InitItem(Item item)
    {
//        Debug.Log("100:" + item.Itemname);
        this.item = item;
 //       Debug.Log("101:" + item.Itemname);
    }

    private void OnEnable()
    {
        menu.YesButton.onClick.AddListener(OnClickYes);
        menu.NoButton.onClick.AddListener(OnClickNo);
        closeButton.onClick.AddListener(OnClickNo);
    }

    private void OnDisable()
    {

        if (isConfirmation)
        {
            cellShop.OnClickBuyAccepted(item);
            controller.UpdateInventory();
        }
    }


    public void OnClickYes()
    {
        isConfirmation = true;
        gameObject.SetActive(false);
    }

    public void OnClickNo()
    {
        isConfirmation = false;
        gameObject.SetActive(false);
//        menu.PanelLevelCompleted.SetActive(true);
    }


}
