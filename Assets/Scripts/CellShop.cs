﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CellShop : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private GameObject enableCell;
    [SerializeField] private Text numberItems;
    [SerializeField] private Text nameItem;
    [SerializeField] private Text cost;
 //   [SerializeField] private Image namePlate;
    [SerializeField] private GameObject messagesPanel;
    [SerializeField] private GameObject warningsPanel;
    /*
    [SerializeField] private GameObject itemDetailsInfo;
    [SerializeField] private Image itemIconInfo;
    [SerializeField] private Text itemNameInfo;
    [SerializeField] private Text itemDescriptionInfo;
    */
    [SerializeField] private Button buyButton;
    [SerializeField] private Image coinImage;
//    [SerializeField] private Button adButton;

    private Color cellBackground;
    public Action<Item> warnMessage;

    public Action updateShopUIInventory;
    private Item item;
    private Item currentItem;
    private int ForcePotionCount;
    private int ArmorPotionCount;
    private int DamagePotionCount;
    int coinCount;
    public Image Icon
    {
        get { return icon; }
        set { icon = value; }
    }
    public Color CellBackground
    {
        get { return cellBackground; }
    }
    public Text NumberItems
    {
        get { return numberItems; }
        set { numberItems = value; }
    }
    public Item Item
    {
        get { return item; }
    }
    public Button BuyButton
    {
        get { return buyButton; }
        set { buyButton = value; }
    }
    public Image CoinImage
    {
        get { return coinImage; }
        set { coinImage = value; }
    }

    private void Awake()
    {

 //       icon.sprite = null;
 //       icon.color = cellBackground;
 //       numberItems.text = null;
 //       cost.text = null;
 //       nameItem.text = null;

 //       ArmorPotionCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount");
 //       ForcePotionCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount");
 //       DamagePotionCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount");
    }

    private void Start()
    {
        buyButton.onClick.AddListener(OnClickBuy);
        //GameManager.Instance.shopInventory.Controller.ItemDetailsInfo.SetActive(false);
    }

    public void InitShopCell(Item item)
    {
        // Debug.Log("cell shop - Init...");
        this.item = item;
        if (item == null)
        {
            enableCell.SetActive(false);
        //    icon.sprite = null;
        //    icon.color = cellBackground;
            numberItems.text = null;
            cost.text = null;
            nameItem.text = null;

            //           namePlate.enabled = false;
        }
        else
        {
            enableCell.SetActive(true);
            icon.sprite = item.Icon;
            icon.color = Color.white;
 //           namePlate.enabled = true;
            cost.text = "" + item.Cost;
            nameItem.text = item.Itemname;

        }
    }
    public void OnClickCell()
    {
        if (item == null)
            return;
        GameManager.Instance.shopInventory.Controller.FillItemDetails(item);
    }

    /*
    public void FillItemDetails(Item item)
    {
        itemIconInfo.sprite = item.Icon;
        itemNameInfo.text = item.Itemname;
        itemDescriptionInfo.text = item.ItemDescription + " (+" + item.Value + ")";
        itemDetailsInfo.SetActive(true);
    }
    */

    public void OnClickBuy()
    {
        Debug.Log("item0:" + item.Itemname);
        if (item == null)
            return;
        if (!IsCoinEnough(item.Cost))
        {
            Debug.Log("No ENOUGH");
            warningsPanel.SetActive(true);
            //return;
        }
        else
        {
//            Debug.Log("item11:" + item.Itemname);
//            Debug.Log(" messagesPanel.GetComponent<ConfirmationMessagesPanel>():" + messagesPanel.GetComponent<ConfirmationMessagesPanel>());

            messagesPanel.GetComponent<ConfirmationMessagesPanel>().InitItem(item);
 //           Debug.Log("item102:" + item.Itemname);
 //           Debug.Log("messagesPanel.activeSelf:" + messagesPanel.activeSelf);

            messagesPanel.SetActive(true);
//            Debug.Log("messagesPanel.activeSelf2:" + messagesPanel.activeSelf);

        }

    }


    public void OnClickBuyAccepted(Item item)
    {
        if (item == null) { Debug.Log("item NULL"); return; }

        AddItemToPlayerInventory(item);
        GameManager.Instance.shopInventory.Items.Remove(item);

        coinCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        coinCount -= item.Cost;
        PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount", coinCount);

        
        Debug.Log("updateShopUIInventory:" + updateShopUIInventory);

        if (updateShopUIInventory != null)
        {
            updateShopUIInventory();
        }
    }

    public void AddItemToPlayerInventory(Item item) {
 //       Debug.Log("AddItemToPlayerInventory:" + item.Itemname);



        if (item.Type == BuffType.Armor) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ArmorPotionCount") + 1);
        if (item.Type == BuffType.Damage) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_DamagePotionCount") + 1);
        if (item.Type == BuffType.Force) PlayerPrefs.SetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount", PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_ForcePotionCount") + 1);
    }

    public bool IsCoinEnough(int costOfItem) {
        coinCount = PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_CoinCount");
        if (costOfItem > coinCount) return false;
        else return true;

    }


}
