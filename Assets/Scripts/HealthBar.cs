﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image healthImage;
    private float healthValue;
    [SerializeField] private float delta;
    private Player plaeyr;
    private float maxHealth;
    private float currentHealth;
    public float MaxHealth {
        get { return maxHealth; }
        set { maxHealth = value; }
    }
    void Start()
    {
        plaeyr = FindObjectOfType<Player>();
        maxHealth = (float)plaeyr.Health.CurrentHealth;
        healthValue = plaeyr.Health.CurrentHealth / maxHealth;
    }

    void Update()
    {
        currentHealth = plaeyr.Health.CurrentHealth / maxHealth;
           if (currentHealth > healthValue) healthValue += delta;
           if (currentHealth < healthValue) healthValue -= delta;
           if (Mathf.Abs(currentHealth - healthValue) < delta) healthValue = currentHealth;
        healthImage.fillAmount = healthValue;
    //    Debug.Log("healthValue:" + healthValue);

   //     if (healthValue <= 0)
   //     {
   //         plaeyr.beforeDestroy();
   //     }
    }
}
