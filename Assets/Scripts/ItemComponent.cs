﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour, IObjectDestroyer
{
    [SerializeField] private ItemType itemType;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] Animator animator;
    private Item item;
    public bool isDestroying = false;
    public SpriteRenderer ItemRenderer
    {
        get { return spriteRenderer; }
        set { spriteRenderer = value; }
    }
    public Item Item
    {
        get { return item; }
        set { item = value; }
    }
    public ItemType ItemType
    {
        get { return itemType; }
        set { itemType = value; }
    }

    void Start()
    {
       //      item = GameManager.Instance.itemDataBase.GetItemOfId((int)itemType);
       //      spriteRenderer.sprite = item.Icon;
       //      GameManager.Instance.itemsContainer.Add(gameObject, this);
        
        InitItemComponent();
    }

    public void InitItemComponent()
    {
        item = GameManager.Instance.itemDataBase.GetItemOfId((int)itemType);
        spriteRenderer.sprite = item.Icon;
        GameManager.Instance.itemsContainer.Add(gameObject, this);
    }
   
    public void Destroy(GameObject gameObject)
    {
        MonoBehaviour.Destroy(gameObject);
    }
    

    public void StartDestroy()
    {
        isDestroying = true;
        animator.SetTrigger("StartDestroy");
    }

    public void EndDestroy()
    {
        Destroy(gameObject);
        isDestroying = false;
    }
}

public enum ItemType
{
    DamagePotion=1,
    ArmorPotion = 2,
    ForcePoition = 3
}

