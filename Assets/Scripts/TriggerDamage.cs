﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamage : MonoBehaviour
{

    [SerializeField] private int damage;
    [SerializeField] private bool isDestroyingAfterCollision;
    private IObjectDestroyer destroyer;
    private int additiveBonuse;

    private BuffEmitter buffEmitter;
    private GameObject parent;
    public GameObject Parent
    {
        get { return parent; }
        set { parent = value; }
    }
    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }
    public int AdditiveBonuse
    {
        get { return additiveBonuse; }
        set { additiveBonuse = value; }
    }

    private void Start()
    {
        GameManager.Instance.triggerDamage = this;
        buffEmitter = gameObject.GetComponent<BuffEmitter>();
    }
    public void Init(IObjectDestroyer destroyer)
    {
        this.destroyer = destroyer;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject == parent) return;
        if (GameManager.Instance.healthContainer.ContainsKey(col.gameObject))
        {
            Debug.Log("Log 1.");
            var health = GameManager.Instance.healthContainer[col.gameObject];

            if (health != null)
            {
                Debug.Log("Log 1.");
                if (CompareTag("Water")) { health.TakeHit(health.CurrentHealth); Global.isFalledInWater = true; }
                else
                     health.TakeHit(damage);
            }
            if (isDestroyingAfterCollision)
            {
                if (destroyer == null)
                    Destroy(gameObject);
                else destroyer.Destroy(gameObject);
            }
        }
    }


    private void OnTriggerExit2D(Collider2D col)
    {
        Animator animator = col.gameObject.GetComponent<Animator>();
        if (col.gameObject.CompareTag("Player"))
        {
            animator.SetTrigger("EndDamage");
        }
    }
}

public interface IObjectDestroyer
{
    void Destroy(GameObject gameObject);
}



