﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPrize : MonoBehaviour
{
    [SerializeField] private Door door;
    private GameObject keyPrize;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;
        GameManager.Instance.audioManager.PlayTakeKey();

        Destroy(gameObject);
        keyPrize = GameManager.Instance.keyPrizeSymbol;
        keyPrize.SetActive(true);

        door.SetOpenedDoorSprite();
    }
}
