﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShopUIController : MonoBehaviour
{
    [SerializeField] private CellShop[] cells;
    [SerializeField] private int cellCount;
    [SerializeField] private CellShop cellPrefab;
    [SerializeField] private Transform rootParent;

    [SerializeField] private GameObject itemDetailsInfo;
    [SerializeField] private Image itemIconInfo;
    [SerializeField] private Text itemNameInfo;
    [SerializeField] private Text itemDescriptionInfo;

    public Action updateCoinCountShopUI;
    private ShopInventory inventory;
    private int currentCellNumber;
    private Color disableButtonColor = new Color(0.3521271f, 0.3582445f, 0.3867925f, 1);
    private Item firstItemInInventory = null;

    public Item FirstItemInInventory
    {
        get { return firstItemInInventory; }
    }

    public GameObject ItemDetailsInfo
    {
        get { return itemDetailsInfo; }
        set { itemDetailsInfo = value; }
    }

    private void Start()
    {

    }
    private void Init()
    {
        cells = new CellShop[cellCount];
        for (int i = 0; i < cellCount; i++)
        {
            cells[i] = Instantiate(cellPrefab, rootParent);
            cells[i].updateShopUIInventory += UpdateInventory;
        }
        cellPrefab.gameObject.SetActive(false);
    }

    public void OnEnable()
    {

        //      Debug.Log("On Enable Grid" + cells);
        if (cells == null || cells.Length <= 0)
            Init();
        UpdateInventory();

        //        inventory.updateShopInventory += UpdateInventory;
    }


    public void UpdateInventory()
    {
        firstItemInInventory = null;
        currentCellNumber = 0;

        if (GameManager.Instance.shopInventory == null) return;
        inventory = GameManager.Instance.shopInventory;
        /*
        Debug.Log("ВСЕГО=" + inventory.Items.Count);
        Debug.Log("GetAmountItemsByType(BuffType.Armor)" + GetAmountItemsByType(BuffType.Armor));
        Debug.Log("GetAmountItemsByType(BuffType.Damage)" + GetAmountItemsByType(BuffType.Damage));
        Debug.Log("GetAmountItemsByType(BuffType.Force)" + GetAmountItemsByType(BuffType.Force));
        */

        fillCellByTypeAndAmount(BuffType.Armor);
        fillCellByTypeAndAmount(BuffType.Damage);
        fillCellByTypeAndAmount(BuffType.Force);

        for (int i = currentCellNumber; i < cells.Length; i++)
            cells[i].InitShopCell(null);

        if (firstItemInInventory == null) itemDetailsInfo.SetActive(false);
        else
            FillItemDetails(firstItemInInventory);

        if (updateCoinCountShopUI != null)
        {
            updateCoinCountShopUI();
        }



    }

    private void fillCellByTypeAndAmount(BuffType type)
    {
        if (GetAmountItemsByType(type) > 0)
        {
            //        Debug.Log("currentCellNumber(type)=" + currentCellNumber);

            cells[currentCellNumber].InitShopCell(FindItemByBuffType(type));
            if (GetAmountItemsByType(type) > 1)
                cells[currentCellNumber].NumberItems.text = "" + GetAmountItemsByType(type);
            else cells[currentCellNumber].NumberItems.text = null;
            if (!cells[currentCellNumber].IsCoinEnough(cells[currentCellNumber].Item.Cost))
            {
                cells[currentCellNumber].BuyButton.gameObject.transform.GetChild(0).GetComponentInChildren<Image>().color = disableButtonColor;
                cells[currentCellNumber].BuyButton.interactable = false;
                cells[currentCellNumber].CoinImage.color = disableButtonColor;
            }
            if (firstItemInInventory == null) firstItemInInventory = cells[currentCellNumber].Item;
            currentCellNumber++;

        }
    }

    private int GetAmountItemsByType(BuffType type)
    {
        int amount = 0;
        for (int i = 0; i < inventory.Items.Count; i++)
            if (inventory.Items[i].Type == type) amount++;
        return amount;
    }

    private Item FindItemByBuffType(BuffType type)
    {
        for (int i = 0; i < inventory.Items.Count; i++)
            if (inventory.Items[i].Type == type) return inventory.Items[i];
        return inventory.Items[0];
    }

    public void FillItemDetails(Item item)
    {
        if (item == null) return;

        itemIconInfo.sprite = item.Icon;
        itemNameInfo.text = item.Itemname;
        itemDescriptionInfo.text = item.ItemDescription + " (+" + item.Value + ")";
        itemDetailsInfo.SetActive(true);

    }


    /*
        public void UpdateInventory()

        {
            if (GameManager.Instance.shopInventory == null) return;
            ShopInventory inventory = GameManager.Instance.shopInventory;
            for (int i = 0; i < inventory.Items.Count; i++)
            {
           //     Debug.Log("cells.Length:" + cells.Length);

                if (i < cells.Length)
                {
    //                Debug.Log("inventory.Items[i].Itemname" + inventory.Items[i].Itemname);
                    cells[i].InitShopCell(inventory.Items[i]);
                }
            }

            for (int i = inventory.Items.Count; i < cells.Length; i++)
                cells[i].InitShopCell(null);

        }
    */

}
