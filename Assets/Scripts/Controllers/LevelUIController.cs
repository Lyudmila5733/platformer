﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelUIController : MonoBehaviour
{
    [SerializeField] private Level[] levels;
    [SerializeField] private int levelCount;
    [SerializeField] private int levelActiveCount;
    [SerializeField] private Level levelPrefab;
    [SerializeField] private Transform rootParent;
 //   [SerializeField] private Text text;
    [SerializeField] private Button button;

    [SerializeField] private Button levelOneButton;
    [SerializeField] private Button levelTwoButton;
    [SerializeField] private Button levelThreeButton;
//    [SerializeField] private SpriteRenderer lockImage;
  //  [SerializeField] private Image locklevelOneImage;
    [SerializeField] private Image locklevelTwoImage;
    [SerializeField] private Image locklevelThreeImage;

    [SerializeField] private Color disableBackColor;
    private Color enableBackColor = Color.white;

//    [SerializeField] private Button levelOneEnableButton;
//    [SerializeField] private Image levelOneEnableImage;
//    [SerializeField] private Button levelOneDisableButton;
//    [SerializeField] private Image levelOneDisableImage;
    //   [SerializeField] private Image image;
    //private Button[] buttons;
    private Button tempButton;

    private void Init()
    {
        /*
        levels = new Level[levelCount];
        buttons = new Button[levelCount];
        for (int i = 0; i < levelCount; i++)
        {
            levels[i] = Instantiate(levelPrefab, rootParent);
            text.text = "" + (i + 2);
 //           Debug.Log("currentLevel" + PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") + "; i + 2 =" + (i + 2));
            
            buttons[i] = rootParent.GetChild(i+1).GetComponent<Button>();

            if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") < (i + 2))
    //        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") < i)
            {
                button.interactable = false;
               // text.color = new Color(0.7264151f, 0.3582445f, 0.3867925f, 1);
            }
        }
        levelPrefab.gameObject.SetActive(false);

    */
        levelOneButton.onClick.AddListener(delegate { LoadScenaById(3); });
        levelTwoButton.onClick.AddListener(delegate { LoadScenaById(4); });
        levelThreeButton.onClick.AddListener(delegate { LoadScenaById(5); });
     //   buttons[3].onClick.AddListener(delegate { LoadScenaById(6); });
     //   buttons[4].onClick.AddListener(delegate { LoadScenaById(7); });
    }

    private void OnEnable()
    {
        if (levels == null || levels.Length <= 0)
            Init();
 //       Debug.Log("a="+ PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel"));
 //       Debug.Log("b="+ locklevelTwoImage.gameObject);
        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") == 1) 
        {
 //           Debug.Log("1!");
            locklevelTwoImage.gameObject.SetActive(true);
            locklevelThreeImage.gameObject.SetActive(true);
            levelTwoButton.interactable = false;
            levelThreeButton.interactable = false;
            levelTwoButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = disableBackColor;
            levelThreeButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = disableBackColor;
        } 

        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") == 2) 
        {
//            Debug.Log("2!");

            locklevelTwoImage.gameObject.SetActive(false);
            locklevelThreeImage.gameObject.SetActive(true);
            levelTwoButton.interactable = true;
            levelThreeButton.interactable = false;
            levelTwoButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = enableBackColor;
            levelThreeButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = disableBackColor;

        } 

        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") == 3) 
        {
//            Debug.Log("3!");

            locklevelTwoImage.gameObject.SetActive(false);
            locklevelThreeImage.gameObject.SetActive(false);
            levelTwoButton.interactable = true;
            levelThreeButton.interactable = true;
            levelTwoButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = enableBackColor;
            levelThreeButton.gameObject.transform.GetChild(0).GetComponent<Image>().color = enableBackColor;

        } 
        if (PlayerPrefs.GetInt(PlayerPrefs.GetString("Player_Name") + "_currentLevel") >3) 

        {
//            Debug.Log(">3");
            locklevelTwoImage.gameObject.SetActive(false);
            locklevelThreeImage.gameObject.SetActive(false);
            levelTwoButton.interactable = true;
            levelThreeButton.interactable = true;
        }
        //     UpdateInventory();
    }

    public void LoadScenaById(int id)
    {
        SceneManager.LoadScene(id);
    }

}
