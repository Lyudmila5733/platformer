﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class InventoryUIController : MonoBehaviour
{
    [SerializeField] private Cell[] cells;
    [SerializeField] private int cellCount;
    [SerializeField] private Cell cellPrefab;
    [SerializeField] private Transform rootParent;
    private PlayerInventory inventory;
    private int currentCellNumber;

    private void Init()
    {
        cells = new Cell[cellCount];
        for (int i = 0; i < cellCount; i++)
        {
            cells[i] = Instantiate(cellPrefab, rootParent);
            cells[i].updateUIInventory += UpdateInventory;
        }
        cellPrefab.gameObject.SetActive(false);
    }

    private void OnEnable()
    {

        if (cells == null || cells.Length <= 0)
            Init();
        UpdateInventory();
    }


    public void UpdateInventory()
    {
        
        currentCellNumber = 0;
        if (GameManager.Instance == null) { Debug.Log("Instance NULL"); return; }
        if (GameManager.Instance.playerInventory == null) { Debug.Log("playerInventory NULL"); return; }
        inventory = GameManager.Instance.playerInventory;
        if (inventory.Items == null) { Debug.Log("inventory.Items NULL"); return; }
        inventory.HealthText.text = "" + Player.Instance.Health.CurrentHealth + "/" + Player.Instance.Health.MaxHealth;
        inventory.SpeedText.text = "" + Player.Instance.Speed;
        inventory.ForceJumpText.text = "" + Player.Instance.JumpForce;
        inventory.DamageText.text = "" + Player.Instance.ArrowPool[0].TriggerDamage.Damage;
        /*
                Debug.Log("ВСЕГО=" + inventory.Items.Count);
                Debug.Log("GetAmountItemsByType(BuffType.Armor)" + GetAmountItemsByType(BuffType.Armor));
                Debug.Log("GetAmountItemsByType(BuffType.Damage)" + GetAmountItemsByType(BuffType.Damage));
                Debug.Log("GetAmountItemsByType(BuffType.Force)" + GetAmountItemsByType(BuffType.Force));
                */


        fillCellByTypeAndAmount(BuffType.Armor);
        fillCellByTypeAndAmount(BuffType.Damage);
        fillCellByTypeAndAmount(BuffType.Force);

        for (int i = currentCellNumber; i < cells.Length; i++)
            cells[i].Init(null);

    }

    private void fillCellByTypeAndAmount(BuffType type)
    {
        if (GetAmountItemsByType(type) > 0)
        {
       //     Debug.Log("currentCellNumber(type)=" + currentCellNumber);

            cells[currentCellNumber].Init(FindItemByBuffType(type));
            if (GetAmountItemsByType(type) > 1)
                cells[currentCellNumber].NumberItems.text = "" + GetAmountItemsByType(type);
            else cells[currentCellNumber].NumberItems.text = null;
            currentCellNumber++;

        }
    }

    private int GetAmountItemsByType(BuffType type)
    {
        int amount = 0;
        for (int i = 0; i < inventory.Items.Count; i++)
            if (inventory.Items[i].Type == type) amount++;
        return amount;
    }

    private Item FindItemByBuffType(BuffType type)
    {
        for (int i = 0; i < inventory.Items.Count; i++)
            if (inventory.Items[i].Type == type) return inventory.Items[i];
        return inventory.Items[0];
    }


    /*
        public void UpdateInventory()
        {
            if (GameManager.Instance.playerInventory == null) return;
            var inventory = GameManager.Instance.playerInventory;
            for (int i = 0; i < inventory.Items.Count; i++)
            {
                Debug.Log("cells.Length:" + cells.Length);
                if (i < cells.Length)
                {
                    cells[i].Init(inventory.Items[i]);
                }
            }
            for (int i = inventory.Items.Count; i < cells.Length; i++)
                cells[i].Init(null);
        }
        */

}
