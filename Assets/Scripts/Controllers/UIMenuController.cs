﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIMenuController : MonoBehaviour
{
    [SerializeField] private Button backButton;
    [SerializeField] private Button goButton;
    [SerializeField] private Button shopButton;
    [SerializeField] private Button levelButton;

//    [SerializeField] private Button continueButton;
    [SerializeField] private LevelResults levelResults;
    [SerializeField] private LevelUIController levelUIController;

    public Button BackButton
    {
        get { return backButton; }
    }
    public Button GoButton
    {
        get { return goButton; }
    }
    public Button ShopButton
    {
        get { return shopButton; }
    }
    public Button LevelButton
    {
        get { return levelButton; }
    }




    public void Start()
    {
        //LevelResults.Instance.InitUIController(this);
        levelResults.InitUIController(this);
    }
}
