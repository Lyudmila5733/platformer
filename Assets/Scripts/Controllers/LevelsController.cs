﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private Level01 level01;

    [SerializeField] GameObject flowerPlatform;
    [SerializeField] GameObject flowerPlatformFlying;
    [SerializeField] GameObject redWolf;
    [SerializeField] GameObject wolf;
    [SerializeField] GameObject shooter;
    [SerializeField] GameObject coinPack;
    [SerializeField] GameObject movePlatform;
    [SerializeField] GameObject chest;
    [SerializeField] GameObject chestKey;
    [SerializeField] Transform parentGameobject;
    /*
    [SerializeField] private int amountOfPlatforms = 0;
    [SerializeField] private int amountOfFlyingPlatforms = 100;
    [SerializeField] private int amountOfRedwolfs = 10;
    [SerializeField] private int amountOfWolfs = 10;
    [SerializeField] private int amountOfShooters = 10;
    [SerializeField] private int amountOfChests = 10;
    */

    public GameObject FlowerPlatform
    {
        get { return flowerPlatform; }
    }
    public GameObject FlowerPlatformFlying
    {
        get { return flowerPlatformFlying; }
    }
    public GameObject RedWolf
    {
        get { return redWolf; }
    }
    public GameObject Wolf
    {
        get { return wolf; }
    }
    public GameObject Shooter
    {
        get { return shooter; }
    }
    public GameObject CoinPack
    {
        get { return coinPack; }
    }
    public GameObject MovePlatform
    {
        get { return movePlatform; }
    }
    public GameObject Chest
    {
        get { return chest; }
    }
    public GameObject ChestKey
    {
        get { return chestKey; }
    }
    public Transform ParentGameobject
    {
        get { return parentGameobject; }
    }
   /* 
    public int AmountOfPlatforms
    {
        get { return amountOfPlatforms; }
    }    
    public int AmountOfFlyingPlatforms
    {
        get { return amountOfFlyingPlatforms; }
    }    
    public int AmountOfRedwolfs
    {
        get { return amountOfRedwolfs; }
    }    
    public int AmountOfWolfs
    {
        get { return amountOfWolfs; }
    }    
    public int AmountOfShooters
    {
        get { return amountOfShooters; }
    }    
    public int AmountOfChests
    {
        get { return amountOfChests; }
    } 

    */
    private void Awake()
    {
        level01.InitLevelsController(this);
        //level01.InitLevelsController(this);
    }



}
