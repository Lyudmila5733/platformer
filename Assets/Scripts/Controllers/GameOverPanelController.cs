﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanelController : MonoBehaviour
{
    [SerializeField] private Button yesButton;
    [SerializeField] private Button noButton;

    public Button Yes
    {
        get { return yesButton; }
    }

    public Button No
    {
        get { return noButton; }
    }

    void Start()
    {
        Player.Instance.InitGameoverController(this);
    }

    private void OnEnable()
    {
        Time.timeScale = 0;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }



}
