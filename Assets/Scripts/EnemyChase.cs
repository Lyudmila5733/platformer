﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private GameObject leftBorder;
    [SerializeField] private GameObject rightBorder;
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private GroundDetection groundDetection;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private PlayerDetection playerDetection;
    bool isRightNavigation = true;
    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    void Update()

    {
        if (!groundDetection.IsGrounded) return;
        if (playerDetection.IsPlayerDetection)
        {
            spriteRenderer.flipX = playerDetection.Direction > 0 ? false : true;
            isRightNavigation = playerDetection.Direction < 0 ? true : false;
            rigidBody.velocity = isRightNavigation ? Vector2.right : Vector2.left;
            rigidBody.velocity *= speed*2;

        }
        else
        {
            if (transform.position.x >= rightBorder.transform.position.x) isRightNavigation = false;
            else if (transform.position.x <= leftBorder.transform.position.x) isRightNavigation = true;
            rigidBody.velocity = isRightNavigation ? Vector2.right : Vector2.left;
            rigidBody.velocity *= speed;
        }



        if (rigidBody.velocity.x >= 0) spriteRenderer.flipX = true;
        if (rigidBody.velocity.x < 0) spriteRenderer.flipX = false;
    }

}
