﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour, IObjectDestroyer
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Rigidbody2D rigidbody;
    [SerializeField] private TriggerDamage triggerDamage;
    [SerializeField] private float force;
    [SerializeField] private float lifeTime;
    private ArrowPool arrowPool;
    private Player player;
    private EnemyCharacter enemyCharacter;
    public TriggerDamage TriggerDamage
    {
        get { return triggerDamage; }
        set { triggerDamage = value; }
    }
    public float Force
    {
        get { return force; }
        set { force = value; }
    }

    public void SetImpulseFromPlayer(Vector2 direction, float force, Player player)
    {
        this.player = player;
        triggerDamage.Parent = player.gameObject;
        triggerDamage.Init(this);
        rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
        if (force < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (force != 0)
            StartCoroutine(LifeTime());

    }

    public void SetImpulseFromEnemy(Vector2 direction, float force, GameObject enemyCharacter)
    {
        this.enemyCharacter = enemyCharacter.GetComponent<EnemyCharacter>();
        triggerDamage.Parent = enemyCharacter.gameObject;
        triggerDamage.Init(this);
        rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
        if (force < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (force != 0)
            StartCoroutine(LifeTime());

    }

    public void Destroy(GameObject gameObject)
    {
        if (player != null)
            player.ReturnArrowToPool(this);
        if (enemyCharacter != null)
            enemyCharacter.GetComponent<ArrowPool>().ReturnArrowToPool(this);

    }

    private IEnumerator LifeTime()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
        yield break;
    }


}
