﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPool : MonoBehaviour
{
    [SerializeField] private Arrow prefabArrow;
    [SerializeField] private Transform arrowSpawnPoint;
    [SerializeField] private float arrowRechargeTime;
//    [SerializeField] private float arrowRechargeTime = 0.8f;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private bool isReadyToShoot = true;
    [SerializeField] private float forceArrow;
    private List<Arrow> arrowPool;
    private byte arrowsCount = 5;
    private Arrow currentArrow;
    public bool IsReadyToShoot
    {
        get { return isReadyToShoot; }
    }

    void Start()
    {
        InitArrowPool();

    }

    private void InitArrowPool()
    {
        if (arrowPool == null)
            arrowPool = new List<Arrow>();
        for (int i = 0; i < arrowsCount; i++)
        {
            var tempArrow = Instantiate(prefabArrow, arrowSpawnPoint);
            tempArrow.gameObject.SetActive(false);
            arrowPool.Add(tempArrow);
            if (currentArrow == null) currentArrow = tempArrow;
        }
    }

    private Arrow GetArrowFromPool()
    {
        if (arrowPool.Count > 0)
        {
            var tempArrow = arrowPool[0];
            arrowPool.Remove(tempArrow);
            tempArrow.gameObject.SetActive(true);
            tempArrow.transform.parent = null;
            tempArrow.transform.position = arrowSpawnPoint.position;
            return tempArrow;
        }
        return Instantiate(prefabArrow, arrowSpawnPoint.position, spriteRenderer.flipX ? Quaternion.Euler(0, 180, 0) : Quaternion.identity);
    }

    public void ReturnArrowToPool(Arrow tempArrow)
    {
        if (!arrowPool.Contains(tempArrow))
        {
            tempArrow.gameObject.SetActive(false);
            tempArrow.transform.parent = arrowSpawnPoint;
            tempArrow.transform.position = arrowSpawnPoint.transform.position;
            arrowPool.Add(tempArrow);
        }
    }
    public void CheckShoot()
    {
        if (isReadyToShoot)
        {
            Animator animator = gameObject.GetComponent<Animator>();
            animator.SetTrigger("StartDamage");
            isReadyToShoot = false;
        }
    }

    private void CreateArrow()
    {
        currentArrow = GetArrowFromPool();
        currentArrow.GetComponent<Rigidbody2D>().isKinematic = true;
        currentArrow.transform.parent = gameObject.transform;
        if (spriteRenderer.flipX) currentArrow.transform.rotation = Quaternion.Euler(0, 180, 0); else currentArrow.transform.rotation = Quaternion.identity;
        currentArrow.SetImpulseFromEnemy(!spriteRenderer.flipX ? Vector2.right : Vector2.left, 0, gameObject);

    }

    public void Shoot()
    {
        currentArrow.GetComponent<Rigidbody2D>().isKinematic = false; //trouble
        currentArrow.transform.parent = null;
        currentArrow.SetImpulseFromEnemy(!spriteRenderer.flipX ? Vector2.right : Vector2.left, forceArrow, gameObject);
        StartCoroutine(RechargeTime());
    }

    private IEnumerator RechargeTime()
    {
        yield return new WaitForSeconds(arrowRechargeTime);
        isReadyToShoot = true;
        yield break;
    }


}
